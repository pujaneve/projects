#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module receives events generated for changes in granular data store.
The processing logic for aggregation kicks in.
The processiong results are stored in aggregate data store and a trigger is placed for scorer to run
"""


__version__ = '0.1'

from kafka import KafkaConsumer, KafkaProducer
from couchbase.cluster import PasswordAuthenticator, Cluster
from couchbase.n1ql import N1QLQuery
import couchbase.subdocument as SD
import couchbase.exceptions as CB_EXCEPTIONS
import logging

# couchbase related variables
DB_HOST = 'localhost'
DB_USER = 'Administrator'
DB_PASSWORD = 'administrator'
GRANULAR_BUCKET = 'granular'
AGGREGATE_BUCKET = 'aggregate'

# kafka related variables
GRANULAR_ALERT_TOPIC = 'GranularAlert'
AGGREGATOR_ALERT_TOPIC = 'AggregatorAlert'
KAFKA_HOST = 'localhost'
KAFKA_PORT = 9092

logger = logging.getLogger(__name__)

if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING)
    logger.setLevel(logging.DEBUG)

    logger.info('Connecting to Couchbase cluster')
    cluster = Cluster('couchbase://' + DB_HOST)
    cluster.authenticate(PasswordAuthenticator(DB_USER, DB_PASSWORD))
    granular_bucket = cluster.open_bucket(GRANULAR_BUCKET)
    aggregate_bucket = cluster.open_bucket(AGGREGATE_BUCKET)

    logger.info('Instantiating Kafka Producer')
    kafka_producer = KafkaProducer(bootstrap_servers='{}:{}'.format(KAFKA_HOST, KAFKA_PORT),
                                   api_version=(0, 10))
    logger.info('Instantiating Kafka consumer for {} topic'.format(GRANULAR_ALERT_TOPIC))
    granular_alert_consumer = KafkaConsumer(GRANULAR_ALERT_TOPIC,
                                            bootstrap_servers='{}:{}'.format(KAFKA_HOST, KAFKA_PORT),
                                            api_version=(0, 10))

    for event in granular_alert_consumer:
        logger.debug('received {}'.format(event.value))
        event_id = event.value.decode()
        query = 'select actor.id as actor_id, repo.id as repo_id, payload.commits from `granular` where type="PushEvent" and meta(granular).id="' + '{}"'.format(event_id)
        logger.debug(query)
        query = N1QLQuery(query)
        for row in granular_bucket.n1ql_query(query):
            actor_id = str(row['actor_id'])
            repo_id = str(row['repo_id'])
            commits = row['commits']
            logger.debug('{}:{}:{}'.format(actor_id, repo_id, commits))
            for commit in commits:
                try:
                    aggregate_bucket.mutate_in(actor_id, SD.array_append(repo_id, commit))
                except CB_EXCEPTIONS.NotFoundError:
                    aggregate_bucket.upsert(actor_id, {repo_id: [commit]})
                except CB_EXCEPTIONS.SubdocPathNotFoundError:
                    aggregate_bucket.upsert(actor_id, {repo_id: [commit]})
            kafka_producer.send(AGGREGATOR_ALERT_TOPIC, str(row['actor_id']).encode())
