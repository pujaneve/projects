#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module implements the functionality of ingesting transformed source data (data obtained after ETL job)

It downloads the output files (result of AWS glue ETL jobs) from S3 and published onto Kafka topic
"""

__version__ = '0.1'

from kafka import KafkaProducer
from aws_functions import *
import logging
from time import sleep
import json

logger = logging.getLogger(__name__)


def send_to_kafka(kafka_instances, kafka_topic, s3_bucket_name):
    """
    Read the output data of ETL jobs. Send the data to kafka topic.
    :param kafka_host: dict<kafka_host:kafka_port>
    :param kafka_topic: name of the queue
    :param s3_bucket_name: name of the bucket where output of Glue jobs are stored
    :return: Nothing
    """
    servers = ''
    for kafka_host in kafka_instances:
        servers = servers + '{}:{},'.format(kafka_host, kafka_instances[kafka_host])
    servers = servers[:-1]  # removing the last unnecessary comma

    logger.info('Instantiating Kafka producer')
    kafka_producer = KafkaProducer(bootstrap_servers=servers,
                             api_version=(0, 10))
    # getting ouputs of the ETL job from S3 objects
    output_generator = get_etl_jobs_output(s3_bucket_name)
    # output_generator = get_output()
    while output_generator:
        try:
            # each iteration will give us output from a S3 object
            content = next(output_generator)
            logger.debug(len(content))
        except StopIteration:
            break

        # push the content obtained into the pipeline
        for j in content.split(b'\n'):
            logger.debug('sending to kafka topic')
            kafka_producer.send(kafka_topic, j)
        # producer.flush()

    # close the producer instance
    kafka_producer.close()


def start_producer(kafka_instances, kafka_topic, s3_bucket_name):
    while True:
        # start the ETL job
        logger.info('starting the etl job')
        job_run_id = start_etl_job('git-archive-events')

        # TODO wait for ETL job to complete
        check_retry = 20
        while status_etl_job('git-archive-events', job_run_id) != 'SUCCEEDED' and check_retry != 0:
            logger.debug('will check the status of glue job in 1 min')
            sleep(60)
            check_retry -= 1
        if check_retry > 0:
            logger.info('ETL job completed')
            send_to_kafka(kafka_instances, kafka_topic, s3_bucket_name)
            logger.info('Data sent to kafka')
        else:
            logger.error('ETL job failed to complete within 20-minutes')
            stop_etl_job(job_run_id)

       # Run Glue ETL jobs every hour
        logger.debug('Waiting {} minute(s) to start Glue ETL job'.format(60))
        sleep(3600)

if __name__ == '__main__':
    # Read config file
    conf = {}
    with open('conf.json', 'r') as fh:
        conf = json.load(fh)

    logging.basicConfig(level=logging.WARNING)
    logger.setLevel(logging.DEBUG)
    start_producer(conf['kafka']['brokers'], conf['kafka']['topics']['transaction'], conf['aws']['job_name'])
