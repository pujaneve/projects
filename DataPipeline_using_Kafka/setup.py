#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This script is setup utility and will be helpful in development and deployment.
"""

__version__ = '0.1'

from distutils.core import setup

setup(name='Customer360',
      version=__version__,
      description='Analytics platform to make sense of customer interactions',
      author=__author__,
      packages=[
          'couchbase',
          'kafka-python',
          'boto3'
      ])
