#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This module receives events generated for changes in scorers data store.
The processing logic for scores kicks in.
The processiong results are stored in scorer data store
"""

__version__ = '0.1'

from kafka import KafkaConsumer, KafkaProducer
from couchbase.cluster import PasswordAuthenticator, Cluster
from couchbase.n1ql import N1QLQuery
import logging

# couchbase related variables
DB_HOST = 'localhost'
DB_USER = 'Administrator'
DB_PASSWORD = 'administrator'
AGGREGATE_BUCKET = 'aggregate'
SCORE_BUCKET = 'score'

# kafka related variables
AGGREGATOR_ALERT_TOPIC = 'AggregatorAlert'
SCORER_ALERT_TOPIC = 'ScorerAlert'
KAFKA_HOST = 'localhost'
KAFKA_PORT = 9092

logger = logging.getLogger(__name__)

if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING)
    logger.setLevel(logging.DEBUG)

    logger.info('Connecting to Couchbase cluster')
    cluster = Cluster('couchbase://' + DB_HOST)
    cluster.authenticate(PasswordAuthenticator(DB_USER, DB_PASSWORD))
    aggregate_bucket = cluster.open_bucket(AGGREGATE_BUCKET)
    score_bucket = cluster.open_bucket(SCORE_BUCKET)

    logger.info('Instantiating Kafka Producer')
    kafka_producer = KafkaProducer(bootstrap_servers='{}:{}'.format(KAFKA_HOST, KAFKA_PORT),
                                   api_version=(0, 10))
    logger.info('Instantiating Kafka consumer for {} topic'.format(AGGREGATOR_ALERT_TOPIC))
    aggregator_alert_consumer = KafkaConsumer(AGGREGATOR_ALERT_TOPIC,
                                              bootstrap_servers='{}:{}'.format(KAFKA_HOST, KAFKA_PORT),
                                              api_version=(0, 10))

    for event_id in aggregator_alert_consumer:
        logger.debug('received {}'.format(event_id.value))
        actor_id = event_id.value.decode()
        query = 'select * from `{}` where meta({}).id="{}"'.format(AGGREGATE_BUCKET, AGGREGATE_BUCKET, actor_id)
        logger.debug(query)
        query = N1QLQuery(query)
        for row in aggregate_bucket.n1ql_query(query):
            logger.debug('db row :{}'.format(row))
            repo = 0
            commits = 0
            d = row['aggregate']
            repo = len(d.keys())
            for repo_id in d:
                commits += len(d[repo_id])
            score_bucket.upsert(str(actor_id), {'commits': commits})
            kafka_producer.send(SCORER_ALERT_TOPIC, event_id.value)
