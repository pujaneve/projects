#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module connects kafka topic to granular data store.

It takes source data from kafka topic and store it in granular data store.
An event is placed on a kafka topic for the data stored.
"""

__version__ = '0.1'

from kafka import KafkaConsumer, KafkaProducer
from couchbase.cluster import Cluster, PasswordAuthenticator
import logging
import json

# couchbase related variables
DB_HOST = 'localhost'
DB_USER = 'Administrator'
DB_PASSWORD = 'administrator'
GRANULAR_BUCKET = 'granular'

# kafka related variables
TRANSACTION_TOPIC = 'Commits'
GRANULAR_ALERT_TOPIC = 'GranularAlert'
KAFKA_HOST = 'localhost'
KAFKA_PORT = 9092

logger = logging.getLogger(__name__)


if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING)
    logger.setLevel(logging.DEBUG)

    logger.info('Instantiating Kafka Consumer')
    kafka_consumer = KafkaConsumer(TRANSACTION_TOPIC, bootstrap_servers='{}:{}'.format(KAFKA_HOST, KAFKA_PORT),
                                   api_version=(0, 10))
    kafka_producer = KafkaProducer(bootstrap_servers='{}:{}'.format(KAFKA_HOST, KAFKA_PORT),
                                   api_version=(0, 10))

    logger.info('Connecting to Couchbase cluster')
    cluster = Cluster('couchbase://' + DB_HOST)
    authenticator = PasswordAuthenticator(DB_USER, DB_PASSWORD)
    cluster.authenticate(authenticator)
    granular_bucket = cluster.open_bucket(GRANULAR_BUCKET)

    logger.info('Starting to receive messages from kafka')
    for recv_msg in kafka_consumer:
        msg = recv_msg.value.decode()
        logger.debug('Received: {}'.format(msg))
        if msg == '':
            continue

        # msg is newline separated json(s) in binary format
        json_objs = msg.split('\n')
        for json_obj in json_objs:
            # load json data
            data = json.loads(json_obj)
            id = data['id']
            del data['id']

            # add data into couchbase
            granular_bucket.upsert(id, data)

            # send event to granular alert
            kafka_producer.send(GRANULAR_ALERT_TOPIC, id.encode())
    kafka_consumer.close()
