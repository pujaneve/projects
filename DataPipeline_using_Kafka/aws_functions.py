#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Helper functions for AWS S3 and AWS Glue Jobs
"""


__version__ = '0.1'

import logging
import boto3
import botocore

# AWS credentials
AWS_ACCESS_KEY = "AKIAJUVELCPMZ4S2UK6Q"
AWS_SECRET_KEY = "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"

logger = logging.getLogger(__name__)


def start_etl_job(job_name):
    """

    Start a Job in AWS Glue
    :param job_name: exact name of the glue job
    :return: id of running instance of the job
    """
    glue_client = boto3.client('glue', region_name='us-east-1', aws_access_key_id=AWS_ACCESS_KEY,
                               aws_secret_access_key=AWS_SECRET_KEY)
    response = glue_client.start_job_run(JobName='git-archive-events')
    run_id = response['JobRunId']
    return run_id


def stop_etl_job(job_run_id):
    """
    Stop a job in AWS Glue
    :param job_run_id: id of running instance of a Glue job
    :return:
    """
    # TODO call AWS glue job stop API
    pass


def status_etl_job(job_name, job_run_id):
    """
    Check the status of AWS glue job [running, started, stopped etc]
    :param job_name: exact name of the glue job
    :return: 'SUCCEEDED' or 'RUNNING' or
    """
    glue_client = boto3.client('glue', region_name='us-east-1', aws_access_key_id=AWS_ACCESS_KEY,
                               aws_secret_access_key=AWS_SECRET_KEY)
    response = glue_client.get_job_run(JobName=job_name, RunId=job_run_id)
    status = response['JobRun']['JobRunState']
    return status


def get_etl_jobs_output(bucket_name):
    """
    A generator function which will read and return every object in the bucket

    :param bucket_name: name of AWS S3 bucket
    :return: Generator Object for contents of S3 objects
    """
    s3 = boto3.resource('s3', aws_access_key_id=AWS_ACCESS_KEY, aws_secret_access_key=AWS_SECRET_KEY)

    logger.debug('Verifying existence of S3 bucket {}'.format(bucket_name))
    # check if the bucket exist
    try:
        s3.meta.client.head_bucket(Bucket=bucket_name)
    except botocore.exceptions.ClientError as e:
        error_code = int(e.response['Error']['Code'])
        if error_code == 404:
            logger.error('S3 bucket {} does not exist'.format(bucket_name))
        raise e

    # read all files from the bucket and yield
    bucket = s3.Bucket(bucket_name)

    for s3_object in bucket.objects.all():
        logger.debug('Downloading contents of S3 object {}'.format(s3_object.key))
        content = s3_object.get()['Body'].read()
        # delete the object
        logger.debug('file {}, size {}'.format(s3_object.key, len(content)))
        logger.debug('deleting object {}'.format(s3_object.key))
        bucket.delete_objects(Delete={'Objects': [{'Key': s3_object.key}]})
        yield content
    raise StopIteration


def get_output():
    """
    A generator function which will read and return every json file in the current working directory

    :return: Generator Object for contents of S3 objects
    """
    import glob
    l = glob.glob('*.json')
    logger.debug(l)
    for filename in l:
        with open(filename, 'rb') as fh:
            data = fh.read()
            yield data
    raise StopIteration


# For unit-testing
if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING)
    logger.setLevel(logging.DEBUG)

    # output = get_etl_jobs_output('git-archive-output')
    output_data = get_output()
    while output_data:
        c = next(output_data)
        print(len(c))
