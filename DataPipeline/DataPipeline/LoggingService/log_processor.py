#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This script is used to  determine which log store to use and initialize it.
"""

__version__ = '0.1'

import queue
from multiprocessing.dummy import Pool as ThreadPool

import botocore.exceptions

from DataPipeline.LoggingService import file_log_store
from DataPipeline.LoggingService.cw_log_store import CWLogStore
from DataPipeline.common import notification
from DataPipeline.common.logging_agent import LoggingHelper
from DataPipeline.conf import config


def send_notification(tenant, service_name, message):
    """

    :param tenant: Tenant ID
    :param service_name: Service Name
    :param message: log message
    :return:
    """
    logger = LoggingHelper(tenant=tenant, service=config.get_value("logging.stream_name")).get_logger()

    try:

        notification_config = notification.get_tenant_send_notification_information(tenant, 'error')
        recipient = notification_config['recipient']
        cc = notification_config['cc']
        template_name = notification_config['template']
        subject = notification_config['subject']

        body = notification.get_email_template(template_name)
        body = str(body).replace("{{service_name}}", service_name)
        body = str(body).replace("{{message}}", message)
        body = str(body).replace("{{tenant_id}}", tenant)
        try:
            notification.send_email_by_smtp(cc=cc, recipients=recipient,
                                            subject=subject, body=body)
            logger.debug("Email notification sent successfully to {},{}.".format(recipient, cc))
        except Exception as e:
            logger.error(e)

    except botocore.exceptions.NoCredentialsError:
        logger.error("Invalid Credentials Unable to Access Aws Resources")

    except KeyError as e:
        logger.error("No Metadata found for  {} notification information".format(e))

    except botocore.exceptions.ClientError as ex:
        if ex.response['Error']['Code'] == 'NoSuchKey':
            logger.error("No Metadata found of tenant {} for  notification information".format(tenant))
        else:
            logger.error(ex)
    except Exception as e:
        logger.error(e)


class LogProcessor(object):
    """ LogProcessor will look at the configuration to
    determine which log store to use and initialize it.
    """

    def __init__(self):
        #  To get config parameter
        self.cloudwatch_switch = config.get_value("logging.cloudwatch_switch")
        self.q = queue.Queue()  # Initialise the Queue
        self.pool = ThreadPool()  # Create thread pool
        self.log_data = None
        self.rev_logs = None
        self.logs = None
        # Send the logs to queue

    def send_to_queue(self, log_data):
        """
        Send input data to the python queue
        :param log_data: input list of logs
        :return:
        """
        self.log_data = log_data
        self.q.put(self.log_data)  # Put the log_data in Queue
        self.pool.apply_async(self.read_queue())

    def read_queue(self):
        """
        Read the logs data from the queue
        :return:
        """
        while not self.q.empty():
            self.rev_logs = self.q.get()
            if self.cloudwatch_switch == "on":
                # Use Cloudwatch Plugin
                # Selected Cloudwatch as a log store
                self.send_to_cw_logstore(self.rev_logs)  # Send to the selected Log Store
            else:
                # Use files as a log store
                # Logs will be stored in files
                self.send_to_file_logstore(self.rev_logs)

    # Send the logs to Log store..
    def send_to_cw_logstore(self, logs):
        """
        Send the input data Cloudwatch logs
        :param logs: Input data received
        :return:
        """
        self.logs = logs
        msgs = self.logs['batch']
        service_name = self.logs['service_name']
        tenant_id = self.logs['tenant_id']
        batch = []

        for msg in msgs:
            timespan = msg['timestamp']
            send_mail = msg['send_mail']
            message = msg['message']

            if message is not None and message is not "":
                batch.append({"message": message, "timestamp": timespan})
                if str(send_mail).lower() == "true":
                    send_notification(tenant_id, service_name, message)

        log_store = CWLogStore()  # Create object of a class
        log_store.submit_batch(batch, tenant_id, service_name)  # Send the batch of messages to Cloudwatch logs

    def send_to_file_logstore(self, logs):
        """
        Save the input data in files
        :param logs: Input data received
        :return:
        """
        self.logs = logs
        batch = self.logs['batch']
        service_name = self.logs['service_name']
        file_log_store.submit_batch(batch, service_name)
