#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This script is used to store logs in Cloudwatch.
"""

__version__ = '0.1'

from botocore.exceptions import ClientError
from DataPipeline.common.logging_agent import LoggingHelper
from DataPipeline.common import aws_helper
from DataPipeline.conf import config


class CWLogStore(object):
    """
    It will save the logs into cloudwatch

    """

    def __init__(self):

        self.sequence_tokens = {}  # Dictionary to store sequence token
        self.max_retries = 5  # No. of times to try to send batch. It should be more than 2
        self.batch = None
        self.log_group = None
        self.log_stream = None
        self.logger = None
        self.cloudwatch_client = aws_helper.get_cw_log_resource()

    def submit_batch(self, batch, log_group, log_stream):
        """
        Send the batch of log messages to Cloudwatch
        :param batch: Batch of log messages
        :param log_group: Name of log group
        :param log_stream: Name of log stream
        :return:
        """
        self.batch = batch
        self.log_group = log_group
        self.log_stream = log_stream
        self._get_config_parameters()  # Get config parameter from S3 file
        self.logger = LoggingHelper(tenant=self.log_group, service=config.get_value("logging.stream_name")).get_logger()

        if len(self.batch) < 1:  # Checking the length of the batch
            return
        # kwargs is the dictionary which contain all the parameters
        kwargs = dict(logGroupName=self.log_group,
                      logStreamName=self.log_stream,
                      logEvents=self.batch)
        # For loop is needed to take sequence token from the response
        for retry in range(self.max_retries):

            try:
                response = aws_helper.send_log(cloudwatch_client=self.cloudwatch_client,
                                               **kwargs)  # Send logs to Cloudwatch

                if response is None or "rejectedLogEventsInfo" in response:
                    self.logger.error("Failed to deliver logs {}".format(response))
                break
            except ClientError as e:
                if e.response.get("Error", {}) \
                        .get("Code") in "InvalidParameterException":
                    self.logger.error("Failed to deliver logs %s", e)
                if e.response.get("Error", {}) \
                        .get("Code") in ("DataAlreadyAcceptedException",
                                         "InvalidSequenceTokenException"):

                    # Take the sequence Token from the response and add it in kwargs
                    kwargs["sequenceToken"] = e.response["Error"]["Message"].rsplit(" ", 1)[-1]

                else:
                    if e.response.get("Error", {}) \
                            .get("Message") in "The specified log group does not exist.":

                        # Create Group
                        aws_helper.create_log_group(self.log_group, cloudwatch_client=self.cloudwatch_client)

                        # Add Retention Policy to the group
                        if self.retention_in_days is not 0:
                            aws_helper.retention_policy(self.log_group, self.retention_in_days,
                                                        cloudwatch_client=self.cloudwatch_client)

                        else:
                            self.logger.info("Retention Policy is set as never expired")

                    if e.response.get("Error", {}) \
                            .get("Message") in "The specified log stream does not exist.":
                        # Create Stream
                        aws_helper.create_log_stream(self.log_group, self.log_stream,
                                                     cloudwatch_client=self.cloudwatch_client)

            except Exception as e:
                self.logger.error("Failed to deliver logs: %s", e)

        return

    def _get_config_parameters(self):
        """
        Get config parameters from config file and assign it to local variables.

        """
        try:
            self.retention_in_days = config.get_value("logging.retention_in_days")
        except KeyError as e:
            self.logger.error(e)
            self.logger.info("Retention is days is not specified in the "
                             " config file so set retention_in_days as never expired")
            self.retention_in_days = 0
