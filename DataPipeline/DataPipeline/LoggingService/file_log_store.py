#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This script is used to  determine which log store to use and initialize it.
"""


__version__ = '0.1'

import logging
import os

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def submit_batch(batch, service_name):
    """
    Save the batch of log messages in files
    :param batch: Batch of log messages
    :param service_name: Name of service
    :return:
    """
    path = "../logs/"
    if not os.path.exists(path):
        os.makedirs(path)
    file = open(path + service_name, "a+")
    for i in range(len(batch)):
        file.write(str(batch[i]) + "\n")
