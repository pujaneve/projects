#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This script is used to create and store data in Cloudwatch.
"""

__version__ = '0.1'

from flask import Flask, request

from DataPipeline.LoggingService.log_processor import LogProcessor
from DataPipeline.common import aws_helper
from DataPipeline.common.api_response import ApiResponse

app = Flask(__name__)

log_processor = LogProcessor()


@app.route('/logging/status', methods=['GET'])
def status():
    """
    GET method checking status of service.
    :return: Status Success/Error
    """
    try:
        return ApiResponse.success(payload=None)
    except Exception as e:
        print(e)
        return ApiResponse.failure(msg=e)


@app.route('/log_messages', methods=['POST'])
def log_messages():
    """
    Send log message to log store
    :return: Response Success/failure
    """
    try:
        log_message = request.get_json(force=True)
        log_processor.send_to_queue(log_message)
        return ApiResponse.success(msg="Success")
    except Exception as e:
        return ApiResponse.failure(msg=e, status_code=400)


@app.route('/metric', methods=['POST'])
def metric():
    try:
        metric_data = request.get_json(force=True)
        metric_name = metric_data['metric_name']
        namespace = metric_data['namespace']
        value = metric_data['value']
        #  Send the data to the metric
        aws_helper.send_to_metric(metric_name, value, namespace)
        return ApiResponse.success(msg="Success")
    except Exception as e:
        return ApiResponse.failure(msg=e, status_code=400)
