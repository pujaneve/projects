from DataPipeline.LoggingService import api
from DataPipeline.conf import config
from DataPipeline.common.health_monitor import HealthMonitor

health_monitor = HealthMonitor(config.get_value("monitoring.log_group"),
                               config.get_value("logging.stream_name"))

if __name__ == '__main__':
    health_monitor.start_monitoring()  # Start health Monitoring
    api.app.run(host=config.get_value("services_host.logging"),
                port=config.get_value("services_port.logging"))
