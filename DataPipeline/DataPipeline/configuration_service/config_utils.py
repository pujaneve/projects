#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This script is used to load scheduling.json and will be helpful in development and deployment.
"""


__version__ = '0.1'

import json

import boto3
import botocore.exceptions
from flask import Flask,request

from DataPipeline.common.api_response import ApiResponse
from DataPipeline.conf import config

config_service_port = config.get_conf_value("config_service_port")
config_bucket = config.get_conf_value("config_bucket")
config_file = config.get_conf_value("config_file_name")


def get_s3_object(bucket_name, key):
    """
    Get S3 Object.
    :param bucket_name: name of the bucket.
    :param key: name of file tn above bucket.
    :return: S3 Object
    """
    s3 = boto3.resource('s3')
    if s3 is None:
        raise Exception('Invalid Credentials')
    try:
        s3_object = s3.Object(bucket_name, key)
        return s3_object
    except botocore.exceptions.ClientError as ex:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the file does not exist.
        error_code = int(ex.response['Error']['Code'])
        if error_code == 404:
            raise Exception('Specific key {} does not exist'.format(key))
        raise e


try:
    config_file = get_s3_object(config_bucket, config_file)
    data = json.loads(config_file.get()['Body'].read())


except Exception as e:
    raise Exception("Error in config file: Invalid JSON Object")


class Config:
    @staticmethod
    def get_value(key):
        try:
            value = data
            for k in str(key).split("."):
                if k in value:
                    value = value[k]
                else:
                    value = None
                    break
            return value
        except Exception:
            raise Exception("Error in config file: Invalid JSON Object")


app = Flask(__name__)


@app.route('/config', methods=['GET'])
def get_config_value():
    """
    GET method checking status of service.
    :return: Status Success/Error
    """
    try:
        key = request.args.get('key')
        pxm_config = Config
        return ApiResponse.success(payload={"value": pxm_config.get_value(key)})
    except Exception as ex:
        return ApiResponse.failure(msg=ex)
