from DataPipeline.configuration_service import config_utils

if __name__ == '__main__':
    config_utils.app.run(port=config_utils.config_service_port, host="0.0.0.0")
