#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This script is contains REST APIs for schema definition service.
"""


__version__ = '0.1'

from flask import Flask, request

from DataPipeline.common.api_response import ApiResponse
from DataPipeline.variable_definition.variable_definition_utils import VariableDefinition
from DataPipeline.conf import config

app = Flask(__name__)


@app.route('/variable_definition/status', methods=['GET'])
def status():
    """
    GET method checking status of service.
    :return: Status Success/Error
    """
    try:
        return ApiResponse.success(payload=None)
    except Exception as e:
        print(e)
        return ApiResponse.failure(msg=e)


@app.route('/variable_definition/query_path', methods=['GET'])
def get_query_path():
    # Fetching Input Params
    tenant = request.args.get('tenant')
    source = request.args.get('source')

    try:
        variable_definition = VariableDefinition(tenant=tenant,
                                                 service=config.get_value("metadata.variable_definition_stream"))
        query_paths = variable_definition.get_query_s3_path(tenant, source)
        return ApiResponse.success(payload=query_paths)
    except Exception as e:
        if "NoSuchKey" in str(e):
            msg = "No Queries found for tenant: {},source: {}" \
                .format(tenant, source)
            return ApiResponse.failure(msg=msg, status_code=400)
        return ApiResponse.failure(msg=e)


@app.route('/variable_definition/update_variable_metadata', methods=['POST'])
def update_variable_metadata():
    try:
        variable_definition = VariableDefinition(service=config.get_value("metadata.variable_definition_stream"))

        variable_definition.start_variable_data_update()
        return ApiResponse.success(msg="Variable data updated successfully")
    except Exception as e:
        return ApiResponse.failure(msg=e)
