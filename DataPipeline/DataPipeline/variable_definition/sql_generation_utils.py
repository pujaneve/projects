#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This script is used to generate a JSON object from Excel Data.
"""

__version__ = '0.1'

import itertools

from DataPipeline.common.logging_agent import LoggingHelper


class SqlGeneration:
    def __init__(self, service=None, tenant=None):
        if tenant is None:
            tenant = "Other"
        if service is None:
            service = "Variable Definition"
        self.logger = LoggingHelper(tenant=tenant, service=service).get_logger()

    def generate_sql(self, variables, tenant):
        """
        This method initialize sql query generation for input json object.
        :param variables: Variables JSON object.
        :param tenant: Id of tenant.
        :return: List of generated queries.
        """
        self.logger.set_tenant(tenant)
        self.logger.info("Generating sql query for variable json object: {}"
                         .format(variables))
        if variables is None:
            self.logger.error("variable object should not be Null")
            return None

        queries = []

        for variable in variables:
            if len(variables[variable]) > 0:
                queries.extend(self.generate_queries(variables[variable]))
        return queries

    def generate_queries(self, variable_obj):
        """
        This method generates queries for specifc case (case1,case2,case3)
        :param variable_obj: Input variable JSON Object.
        :return: List of queries for specific case.
        """
        if variable_obj is None:
            return None
        generated_queries = []
        for x in variable_obj:
            query = self.generate_query_for_obj(x)
            if query is not None:
                generated_queries.append(query)

            return generated_queries

    def generate_query_for_obj(self, obj):
        """
        This method generate sql query for specific variable object.
        :param obj: Input variable object.
        :return: Generated Query.
        """
        if obj is None:
            self.logger.error("Obj should not be Null")
            return None
        statements = self.generate_select_statements(obj['variable'])
        query = obj['query']
        intermediate = obj['intermediate_queries']

        if intermediate is not None:
            intermediate_query = self.generate_query_for_obj(intermediate)
            query = str(query).replace('{intermediate_table}', intermediate_query)
        if statements is not None and query is not None:
            all_statements = ''
            for statement in statements:
                all_statements = all_statements + statement + ','
            all_statements = all_statements[:-1]  # removing the last unnecessary comma
            query = str(query).replace('{statements}', all_statements)

        self.logger.debug("Generated query: {}".format(query))
        return query

    def generate_select_statements(self, variable_list):
        """
        This method generate select statements for sql query.
        :param variable_list: List of variable objects.
        :return: List of select statements.
        """
        self.logger.debug("Generating Select statement for variables: {}"
                          .format(variable_list))
        if variable_list is None:
            self.logger.error("Variable_obj should not be Null")
            return None

        statements = []
        for variable in variable_list:
            itr_keys = []
            itr_values = []
            for itr in variable['iterations']:
                itr_values.append(variable['iterations'][itr])
                itr_keys.append(itr)

            for combination in itertools.product(*itr_values):
                i = 0
                eff_key = str(variable['key'])
                eff_query = str(variable['statement'])
                for x in combination:
                    eff_key = eff_key.replace('{' + itr_keys[i] + '}', str(x), 10)

                    if itr_keys[i] == "flag" and "{#flag}" in eff_query:
                        eff_query = str(eff_query).replace('{#flag}', str(variable['flags'][str(x)]), 10)
                    eff_query = str(eff_query).replace('{' + itr_keys[i] + '}', str(x), 10)
                    i = i + 1
                statements.append(eff_query + ' AS ' + eff_key)

        return statements
