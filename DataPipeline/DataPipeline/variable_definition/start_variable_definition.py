from DataPipeline.common.health_monitor import HealthMonitor
from DataPipeline.conf import config
from DataPipeline.variable_definition import variable_definition_service
from DataPipeline.variable_definition.variable_definition_utils import VariableDefinition

health_monitor = HealthMonitor(config.get_value("monitoring.log_group"),
                               config.get_value("metadata.variable_definition_stream"))

if __name__ == '__main__':
    variable_definition = VariableDefinition(service=config.get_value("metadata.variable_definition_stream"))
    variable_definition.start_variable_data_update()
    health_monitor.start_monitoring()
    variable_definition_service.app.run(port=config.get_value("services_port.variable_definition"),
                                        host=config.get_value("services_host.variable_definition"))
