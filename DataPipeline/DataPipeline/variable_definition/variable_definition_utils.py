#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This script is used to generate a JSON object from Excel Data.
"""


__version__ = '0.1'

import json
import os
import tempfile

import pandas as pd

from DataPipeline.common import aws_helper, input_validation
from DataPipeline.common.logging_agent import LoggingHelper
from DataPipeline.conf import config
from DataPipeline.variable_definition import sql_generation_utils


class VariableDefinition:
    def __init__(self, service=None, tenant=None):
        if tenant is None:
            tenant = "Other"
        if service is None:
            service = "Variable Definition"
        self.tenant = tenant
        self.service = service
        self.aggregation = ["360", "daily", "360_step4"]
        self.logger = LoggingHelper(tenant=self.tenant, service=self.service).get_logger()

    @staticmethod
    def get_iterators(variable_id, tenant_id, excel_file):
        """
        This method create iteration object  for variable id from
        tenant_specific_iterations sheet.
        :param variable_id: Id of variable in sheet.
        :param tenant_id: Id of tenant in sheet
        :param excel_file: Excel file object.
        :return: iteration object for variable id.
        """

        iterators = excel_file.parse('tenant_specific_iterations')
        iterations = {}
        for index, row in iterators.iterrows():
            if str(row['tenant_id']) == str(tenant_id) and str(variable_id) in str(row['variable_id']).split(","):
                for itr in str(row["iterators"]).split('|'):

                    iterations[str.lower(itr)] = []
                    for x in str(row[str.lower(itr)]).split('|'):
                        iterations[str.lower(itr)].append(str.lower(x))

        return iterations

    @staticmethod
    def get_flag(flag_id, excel_file):
        """
        This method fetch row from flags sheet for input flag id.
        :param flag_id: Id of flag in sheet
        :param excel_file: Excel file object.
        :return: flag row.
        """
        flags = excel_file.parse('flags')
        for index, row_v in flags.iterrows():
            if flag_id == str(row_v["id"]):
                return row_v

    def generate_variable_json(self, source, tenant_id, excel_file, aggregation_type):
        """
        This Method generate variable json object for specific tenant and source.
        :param source: name of source (email/transaction..)
        :param tenant_id: Id of tenant.
        :param excel_file: Excel file object.
        :param aggregation_type: Type of aggregation (Daily/360)
        :return: variable json object.
        """

        queries = excel_file.parse('queries')

        # Create default json object.
        op_json = {'case1': [], 'case2': [], 'case3': []}

        query_dict = {}

        # Iterating over rows in queries sheet.
        for index, row in queries.iterrows():
            # Filter row by subject.
            if row["subject"] == source and str(row["aggregation"]) == str(aggregation_type):

                q_id = row["id"]
                query_obj = {}
                # check if row has intermediate query.
                if q_id in query_dict:
                    query_obj = query_dict[q_id]
                else:
                    query_obj["intermediate_queries"] = None
                    query_obj["variable"] = []

                # Get case to which query belong (case1/case2/case3).
                case = str(row["level"]).lower().replace(" ", "", 1000)
                q = row["query"]

                query_obj["case"] = case
                query_obj["query"] = q

                # Check query has parent query.
                if row["parent_query_id"] != 0:
                    query_obj = {}
                    if q_id in query_dict:
                        query_obj = query_dict[row["parent_query_id"]]
                    # Fetching variables for query.
                    intm_query_obj = {"query": q, "intermediate_queries": None,
                                      "variable": self.get_variables(q_id, tenant_id, excel_file, aggregation_type)}
                    if len(intm_query_obj["variable"]) > 0:
                        query_obj["intermediate_queries"] = intm_query_obj
                        query_dict[row["parent_query_id"]] = query_obj
                else:
                    query_obj["variable"] = self.get_variables(q_id, tenant_id, excel_file, aggregation_type)
                    if len(query_obj["variable"]) > 0:
                        query_dict[q_id] = query_obj
        for key in query_dict:
            op_json[query_dict[key]['case']].append(query_dict[key])
        return op_json

    def get_variables(self, q_id, tenant_id, excel_file, aggregation_type):
        """
        This method generate list of variable to be calculate for input q_id.
        :param q_id: query_id in variable_definitions sheet.
        :param tenant_id: tenant_id in variable_definitions sheet
        :param excel_file: Excel file object.
        :param aggregation_type: Type of aggregation (Daily/360)
        :return: json list of variables.
        """
        variable_definitions = excel_file.parse('variable_definitions')
        variable_list = []

        for index, row_v in variable_definitions.iterrows():
            if q_id == row_v[aggregation_type + "_query_id"]:
                variable = {"key": row_v["name"], "iterations": self.get_iterators(row_v["id"], tenant_id, excel_file)}
                if len(variable["iterations"]) > 0:
                    variable['flags'] = {}
                    if 'flag' in variable["iterations"]:
                        variable['flags'] = {}
                        flags = []
                        for flg in variable["iterations"]['flag']:
                            flag_row = self.get_flag(str(flg), excel_file)
                            flags.append(flag_row['name'])
                            variable['flags'][flag_row['name']] = flag_row['statement'].replace("@VALUES",
                                                                                                flag_row['values'])
                        variable["iterations"]['flag'] = flags
                    variable["statement"] = row_v["statement"]
                    variable_list.append(variable)

        return variable_list

    def update_variable_data(self, excel_file):
        """
        This method update varibale data of each tenant present in excel sheet.
        :param excel_file: Excel file object.
        :return:
        """

        self.logger.info("Starting update of variable data")
        tenant = excel_file.parse('tenant_data')
        tenant_source = excel_file.parse('tenant_source_mapping')
        notification_data = excel_file.parse('notification')

        s3_resource = aws_helper.get_s3_resource()
        for index, row_v in tenant.iterrows():
            tenant_id = row_v['tenant_id']
            tenant_assigned_id = row_v['tenant_assigned_id']
            data_bucket = row_v['bucket']

            for i, row_n in notification_data.iterrows():
                if tenant_id == row_n['tenant_id']:
                    recipient = str(row_n['recipients']).split(",")
                    subject = row_n['subject']
                    cc = str(row_n['cc']).split(",")
                    notification_type = row_n['type']
                    template = str(row_n['email_template'])
                    self.add_notification_metadata(tenant=tenant_assigned_id, recipient=recipient,
                                                   notification_type=notification_type,
                                                   cc=cc, subject=subject, template=template)

            for index2, row_ts in tenant_source.iterrows():
                if tenant_id == row_ts['tenant_id']:
                    brands = str(row_ts['brand']).split(",")
                    source = row_ts['source']
                    events = str(row_ts['event']).split(",")

                    self.logger.info(
                        "Generating variables json object for tenant {},source {}".format(tenant_assigned_id, source))
                    try:
                        for aggr in self.aggregation:
                            json_data = json.loads(str(
                                json.dumps(self.generate_variable_json(source, tenant_id, excel_file, aggr),
                                           ensure_ascii=False)))

                            self.logger.info(
                                "Generated variables json object for tenant {},source {} is {}".format(
                                    tenant_assigned_id,
                                    source,
                                    json_data))
                            sql_generation = sql_generation_utils.SqlGeneration(tenant=self.tenant,
                                                                                service=self.service)
                            result = {"queries": sql_generation.generate_sql(json_data, tenant_assigned_id)}
                            key = self.generate_query_path(tenant_assigned_id, source, aggr)
                            metadata_bucket_name = config.get_value("metadata.bucket_name")
                            self.logger.info(
                                "Updating variable data in metadata store for tenant {},source {}.".format(
                                    tenant_assigned_id,
                                    source))
                            aws_helper.put_s3_object(metadata_bucket_name, key, s3_resource,
                                                     json.dumps(result, ensure_ascii=False))
                            self.logger.info(
                                "Variable data updated in metadata store for tenant {},source {}.".format(
                                    tenant_assigned_id,
                                    source))

                        self.update_tenant_metadata(tenant_id, tenant_assigned_id, source, data_bucket, excel_file,
                                                    brands, events)
                    except Exception as e:
                        self.logger.error(e)
                        raise e

    def update_tenant_metadata(self, tenant_id, assigned_tenant_id, source, data_bucket, excel_file, brands, events):
        """
        This Method update metadata of specific tenant and source.
        :param tenant_id: Tenant Id in excel sheet.
        :param assigned_tenant_id: Assigned Tenant id.
        :param source: Name of source (e.g. email/transaction).
        :param data_bucket: Name of bucket assigned to tenant.
        :param excel_file: Excel file object.
        :param brands: List of the brands.
        :param events: List of the emain
        :return:
        """

        keys = excel_file.parse('mandatory_keys_mapping')
        for index, row_v in keys.iterrows():
            try:
                row_tenant_id = str(row_v['tenant_id'])
                row_source = str(row_v['source'])
                if str(tenant_id) == row_tenant_id and str(source) == row_source:
                    table = str(row_v['table'])
                    for brand in brands:
                        for event in events:
                            base_data_path = join_path("s3a://" + data_bucket, brand, source, event, table)
                            schema_metadata = {
                                "base_data_path": base_data_path,
                                "compatible_schema_versions": [],
                                "current_schema_version": None,
                                "incompatible_schema_versions": [],
                                "latest_version_no": 0,
                                "mandatory_fields": []
                            }
                            for key in str(row_v['primary_keys']).split(","):
                                pm_key = self.get_primary_key(key, excel_file)
                                if pm_key is not None:
                                    schema_metadata['mandatory_fields'].append(pm_key)

                            self.add_schema_metadata(assigned_tenant_id, row_source, event, table, schema_metadata,
                                                     brand)
            except Exception as e:
                self.logger.error(
                    "Error {} in updating metada of tenant {}, source {}.".format(e, assigned_tenant_id, source))

    @staticmethod
    def get_primary_key(key_id, excel_file):
        """
        This method fetch keys from mandatory_keys based on its id.
        :rtype: int
        :param key_id: Id of key in sheet.
        :param excel_file: Excel file object.
        :return: key object.
        """
        keys = excel_file.parse('mandatory_keys')
        sample_obj = {
            "metadata": {},
            "name": None,
            "nullable": True,
            "type": None
        }
        for index, row_v in keys.iterrows():

            if str(key_id) == str(row_v['id']):
                sample_obj['name'] = row_v['name']
                sample_obj['type'] = row_v['type']
                return sample_obj
        return None

    @staticmethod
    def generate_query_path(tenant, source, aggregation):
        return join_path(config.get_value("metadata.variable_data_storage_dir_name"),
                         str(tenant), str(aggregation), str(source) + ".json")

    def get_query_s3_path(self, tenant, source):
        self.logger.info("Fetching Query path for tenant {} and source {}".format(tenant, source))
        if not input_validation.validate(tenant) or not input_validation.validate(source):
            self.logger.warning("Input Params i.e. Tenant and source should not be empty.")
            raise Exception("Input Params i.e. Tenant and source should not be empty.")

        metadata_bucket_name = config.get_value("metadata.bucket_name")
        query_paths = {}
        for aggr in self.aggregation:
            file_path = self.generate_query_path(tenant, source, aggr)
            s3_resource = aws_helper.get_s3_resource()
            if aws_helper.key_existing_s3(s3_resource, metadata_bucket_name, file_path):
                query_paths[aggr] = join_path("s3a://" + metadata_bucket_name, file_path)
                self.logger.info("Successfully fetched queries path for tenant {} and source {}".format(tenant, source))

            else:
                self.logger.warning("No Queries found for tenant {} and source {}".format(tenant, source))
                raise Exception("No SQL Queries found for tenant {} and source {}".format(tenant, source))

        return query_paths

    def add_schema_metadata(self, tenant=None, source=None, event=None, table=None, new_metadata=None, brand=None):
        """
        Update schema metadata for tenant,source,event,table.
        :param tenant: Id of tenant.
        :param source: Name of data source e.g. omniture/responsys.
        :param event: Name of event type.
        :param table: Name of table.
        :param new_metadata: metadata to bbe update.
        :param brand : Name of brand.
        :return: list of tables with datapath in json format.
        """

        if tenant is None or source is None or event is None or table is None or brand is None:
            self.logger.warning("Tenant, brand, source, event, table and new schema should not be empty.")
            raise Exception("Input Params i.e. Tenant, brand, source, event, and table should not be empty.")
        try:
            if not input_validation.validate_schema_metadata(new_metadata):
                self.logger.warning("Invalid schema Metadata.")
                raise Exception("Invalid schema Metadata.")
            metadata_path = join_path(config.get_value("metadata.schema_metadata_storage_dir_name"), tenant, brand,
                                      source,
                                      event, table + ".json")
            # Updating Metadata
            new_metadata['tenant'] = tenant
            new_metadata['source'] = source
            new_metadata['event'] = event
            new_metadata['table'] = table
            new_metadata['brand'] = brand

            s3_resource = aws_helper.get_s3_resource()
            meta_data_bucket_name = config.get_value("metadata.bucket_name")
            if not aws_helper.key_existing_s3(s3_resource, meta_data_bucket_name, metadata_path):
                aws_helper.put_s3_object(meta_data_bucket_name, metadata_path, s3_resource,
                                         json.dumps(new_metadata, indent=4, sort_keys=True))
                self.logger.info(
                    f"Successfully added schema metadata for source: {source},event: {event}, table: {table}")
        except Exception as e:
            self.logger.error(e)
            raise e

    def add_notification_metadata(self, tenant=None, recipient=None, notification_type=None, cc=None, subject=None,
                                  template=None):

        if cc is None:
            cc = []
        if recipient is None:
            recipient = []

        try:
            if not input_validation.validate(tenant):
                self.logger.warning("Invalid tenant.")
                raise Exception("Invalid tenant.")
            metadata_path = join_path(config.get_value("metadata.notification_metadata_storage_dir_name"),
                                      tenant, str(notification_type) + ".json")
            # Updating Metadata
            metadata = {'tenant': tenant, 'recipient': recipient, 'cc': cc, 'subject': subject, 'template': template}

            s3_resource = aws_helper.get_s3_resource()

            meta_data_bucket_name = config.get_value("metadata.bucket_name")
            aws_helper.put_s3_object(meta_data_bucket_name, metadata_path, s3_resource,
                                     json.dumps(metadata, indent=4, sort_keys=True))
            self.logger.info(f"Successfully added notification metadata for tenant: {tenant}")
        except Exception as e:
            self.logger.error(e)
            raise e

    def start_variable_data_update(self):
        metadata_bucket_name = config.get_value("metadata.bucket_name")
        excel_file_key = config.get_value("metadata.variable_data_excel_name")
        s3 = aws_helper.get_s3_resource()
        temp_file, filename = tempfile.mkstemp()
        my_bucket = s3.Bucket(metadata_bucket_name)
        my_bucket.download_file(excel_file_key, filename)
        excel_file_obj = pd.ExcelFile(filename, encoding='utf8')
        self.update_variable_data(excel_file_obj)
        os.remove(filename)

    def start_variable_data_update_local(self):
        excel_file_obj = pd.ExcelFile("/home/raghav/Downloads/Variable_Definition_Excel_Sheet-v1.xlsx", encoding='utf8')
        self.update_variable_data(excel_file_obj)


def join_path(*args):
    output_path = ""
    for i in args:
        output_path = output_path + "/" + i
    return output_path[1:]


if __name__ == '__main__':
    variable_definition = VariableDefinition(service=config.get_value("metadata.variable_definition_stream"))

    variable_definition.start_variable_data_update_local()
#     print(variable_definition.get_query_s3_path("dev", "email"))
