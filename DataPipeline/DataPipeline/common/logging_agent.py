#!/usr/bin/env python
# -*- coding: utf-8 -*-


__version__ = '0.1'

import logging

from DataPipeline.common.logger import Logger
from DataPipeline.conf import config

logging_service_end_point = config.get_value("services_end_point.logging")


class LoggingHelper:
    def __init__(self, tenant="Other", service="Stream"):
        self.tenant = tenant
        self.service = service

    def get_logger(self):
        if logging_service_end_point is None:
            return self.get_default_logger()
        else:
            url = logging_service_end_point + "/log_messages"
            metric_conf_url = logging_service_end_point + "/metric"
            logger = Logger(
                service=self.service,
                url=url,
                metric_url=metric_conf_url,
                tenant_id=self.tenant)
            return logger

    def get_default_logger(self):
        default_logger = logging.getLogger(self.service)
        default_logger.setLevel(logging.DEBUG)
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')
        return default_logger
