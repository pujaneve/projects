#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Helper functions for sending notification through sms client, email client and others client.
"""

__version__ = '0.1'

import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from DataPipeline.common import aws_helper
from DataPipeline.conf import config


def get_email_template(template_name):
    """
    Get email template by name.
    :param template_name: Name to template to be fetch.
    :return: email template content.
    """
    try:
        s3 = aws_helper.get_s3_resource()
        file = aws_helper.get_s3_object(config.get_value("metadata.bucket_name"),
                                        join_path(config.get_value("metadata.email_template_storage_dir_name"),
                                                  template_name), s3)
        return file.get()['Body'].read().decode()
    except Exception as e:
        raise e


def get_tenant_send_notification_information(tenant_id, notification_type):
    """
    Get email template by name.
    :param tenant_id: Id of tenant.
    :param notification_type: Type of email notification.
    :return: tenant specific send notification data.
    """
    try:
        s3 = aws_helper.get_s3_resource()
        file = aws_helper.get_s3_object(config.get_value("metadata.bucket_name"),
                                        join_path(config.get_value("metadata.notification_metadata_storage_dir_name"),
                                                  tenant_id, notification_type + ".json"), s3)
        data = json.loads(file.get()['Body'].read())

        return data
    except Exception as e:
        raise e


def send_email_notification(recipients=None, cc=None, subject=None, body=None):
    sender = config.get_value('notification.sender_email')
    aws_helper.send_email(sender, recipients, cc, subject, body)


def send_email_by_smtp(recipients=None, cc=None, subject=None, body=None):
    """
    Send Email
    :param recipients: Recipient's mail id
    :param cc: Recipient's mail id who will be in CC
    :param subject: Mail subject
    :param body: Mail body
    :return:
    """
    sender_email = config.get_value('notification.sender_email')
    sender_password = config.get_value('notification.sender_password')
    domain = config.get_value('notification.domain')

    # Check recipients and cc is list or not, if not a list then ,make it a list
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = sender_email
    msg['To'] = ','.join(recipients)
    msg['Cc'] = ','.join(cc)
    payload = MIMEText(body, 'html')  # Pass body data as a HTML data
    msg.attach(payload)
    toaddrs = recipients + cc
    domain_config = config.get_value('smtp_domains.' + domain)
    server_name = domain_config['server_host']
    server_host = domain_config['server_port']
    try:
        server = smtplib.SMTP(server_name, server_host)
        server.starttls()
        server.ehlo()
        # Login Credentials for sending the mail
        server.login(sender_email, sender_password)
        server.sendmail(sender_email, toaddrs, msg.as_string())
        server.quit()
    except smtplib.SMTPException as e:
        raise e
    except Exception as e:
        raise e


def join_path(*args):
    output_path = ""
    for i in args:
        output_path = output_path + "/" + i
    return output_path[1:]
