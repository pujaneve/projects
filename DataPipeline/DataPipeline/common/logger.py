#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""This script is for getting log messages from any micro service """

__version__ = '0.1'

import json
import logging
import queue
import threading
import time
import requests


class LogHandler(logging.Handler):
    """
    LogHandler is the constructor.
    It is a type of function that is called automatically whenever an object of Logger class is created.

    """
    END = 1
    FLUSH = 2

    # extra size of meta information with each messages
    EXTRA_MSG_PAYLOAD_SIZE = 26

    def __init__(self, tenant_id, service_name, url, logger,
                 use_queues=True,
                 send_interval=20, max_batch_size=1024 * 1024,
                 max_batch_count=10000, *args, **kwargs):
        """
        :param tenant_id:
            It is the unique id per customer
            :type tenant_id: String
        :param service_name:
            Name of the Micro service.
            :type service_name: String
        :param url:
            Url of logging API server
        :param use_queues:
            If **True**, logs will be queued on a
            per-service basis and sent in batches.
             To manage the queues, a queue
            handler thread will be spawned.
            :type queue: Boolean
        :param send_interval:
            Maximum time (in seconds, or a timedelta) to hold messages in
            queue before sending a batch.
            :type send_interval: Integer
        :param max_batch_size:
            Maximum size (in bytes) of the queue before sending a batch.
            :type max_batch_size: Integer
        :param max_batch_count:
            Maximum number of messages in the queue before sending a batch.
            :type max_batch_count: Integer

        """

        # Initializes the Handler instance
        logging.Handler.__init__(self, *args, **kwargs)

        self.tenant_id = tenant_id
        self.service_name = service_name
        self.url = url
        self.use_queues = use_queues
        self.send_interval = send_interval
        self.max_batch_size = max_batch_size
        self.max_batch_count = max_batch_count
        self.queues = {}
        self.threads = []
        self.shutting_down = False
        self.default_logger = logger
        self.queue_name = service_name + tenant_id

    def set_tenant(self, tenant):
        self.tenant_id = tenant
        self.queue_name = self.service_name + tenant

    def emit(self, message):
        """
        This function takes to actually log the specified logging record
        :param message: This is the log message

        """
        service_name = self.service_name

        # Dictionary is used as the operand to a string formatting operations

        msg_obj = json.loads(message.msg)

        msg = msg_obj['message']
        send_mail = msg_obj['send_mail']
        message.msg = msg
        log_message = dict(timestamp=int(message.created * 1000),
                           message=self.format(message), send_mail=send_mail)
        # If use_queues is set "True"
        if self.use_queues:

            if self.queue_name not in self.queues:
                queue_name = self.queue_name

                self.queues[queue_name] = queue.Queue()
                self.queues[queue_name].put(log_message)

                thread = threading.Thread(target=self.batch_sender,
                                          args=(self.queues[queue_name],
                                                service_name,
                                                self.send_interval,
                                                self.max_batch_size,
                                                self.max_batch_count,
                                                self.tenant_id))

                self.threads.append(thread)
                thread.daemon = True
                thread.start()

            else:
                # To put log_message to queue
                self.queues[self.queue_name].put(log_message)

        else:
            # If use_queue is set "False" then directly call the submit batch
            self._submit_batch([log_message], service_name, self.tenant_id)

    def batch_sender(self, my_queue, service_name, send_interval,
                     max_batch_size, max_batch_count, tenant):

        msg = None

        def size(_msg):
            return (len(_msg["message"]) if isinstance(_msg, dict) else 1) + \
                   LogHandler.EXTRA_MSG_PAYLOAD_SIZE

        def truncate(_msg2):
            _msg2["message"] = _msg2["message"][:max_batch_size - LogHandler.EXTRA_MSG_PAYLOAD_SIZE]
            return _msg2

        while msg != self.END:

            cur_batch = [] if msg is None or msg == self.FLUSH else [msg]

            cur_batch_size = sum(size(msg) for msg in cur_batch)

            cur_batch_msg_count = len(cur_batch)

            cur_batch_deadline = time.time() + send_interval

            while True:
                try:
                    msg = my_queue.get(block=True,
                                       timeout=max(0,
                                                   cur_batch_deadline - time
                                                   .time()))

                    if size(msg) > max_batch_size:
                        msg = truncate(msg)

                except queue.Empty:
                    # If the queue is empty, we don't want
                    # to reprocess the previous message
                    msg = None
                if msg is None \
                        or msg == self.END \
                        or msg == self.FLUSH \
                        or cur_batch_size + size(msg) > max_batch_size \
                        or cur_batch_msg_count >= max_batch_count \
                        or time.time() >= cur_batch_deadline:
                    self._submit_batch(cur_batch, service_name, tenant)
                    if msg is not None:
                        # We don't want to call task_done if the
                        # queue was empty and we didn't receive anything new
                        my_queue.task_done()
                    break
                elif msg:
                    cur_batch_size += size(msg)
                    cur_batch_msg_count += 1
                    cur_batch.append(msg)
                    my_queue.task_done()

    def _submit_batch(self, batch, service_name, tenant):

        """
        This submit batch function is for send list of log messages to server
        :param batch: List of log messages which will send to logging API server
        :param service_name: Name of micro service
        """
        # Check length of batch
        if len(batch) < 1:
            return

        # Combine log messages with service name
        log_msg = {
            'batch': batch,
            'tenant_id': tenant,
            'service_name': service_name
        }

        # dumps list of log messages to log
        list_log_msg = json.dumps(log_msg)

        """Invoke API for logging"""
        try:
            r = requests.post(self.url, data=list_log_msg)
            r.raise_for_status()

        except requests.exceptions.ConnectionError:
            self.default_logger.error(list_log_msg)
            self.default_logger.error("API Connection error occurred.")

        except requests.exceptions.Timeout:
            self.default_logger.error(list_log_msg)
            self.default_logger.error("Request timed out.")

        except requests.exceptions.HTTPError:
            self.default_logger.ex(list_log_msg)
            self.default_logger.error("Request HTTP error")

        except Exception as e:
            self.default_logger.error(list_log_msg)
            self.default_logger.error(e)
        return

    def flush(self):
        for q in self.queues.values():
            q.put(self.FLUSH)
        for q in self.queues.values():
            q.join()

    def close(self):
        self.shutting_down = True
        for q in self.queues.values():
            q.put(self.END)
        for q in self.queues.values():
            q.join()
        logging.Handler.close(self)


def singleton(the_class):
    """ decorator for a class to make a singleton out of it """
    class_instances = {}

    def get_instance(*args, **kwargs):
        """ creating or just return the one and only class instance.
            The singleton depends on the parameters used in __init__ """

        key = (the_class, args, str(kwargs))
        if key not in class_instances:
            class_instances[key] = the_class(*args, **kwargs)
        return class_instances[key]

    return get_instance


# The Singleton class
@singleton
class Logger:
    """
    Singleton class which is a way to provide one and only one object of a particular type.
    Here used the Logger class,it is the logger class for the micro service developers.

    """

    def __init__(
            self,
            url,
            metric_url,
            tenant_id="Other",
            service="Stream"):
        """
        :param url: Address of logging server
        :param metric_url: Address of monitoring server
        :param tenant_id:It is the unique id per customer
        :param service: Name of any micro service which will give by the service developer.
        """

        ''' LogHandler is the constructor of the Logger class
            log_handler is the object of LogHandler '''

        # Initialize parameters
        self.url = url
        self.metric_url = metric_url
        self.tenant_id = tenant_id
        self.service_name = service
        self.metric_name = None
        self.value = None
        self.queue_name = service + tenant_id

        self.default_logger = logging.getLogger(__name__)

        self.default_logger.setLevel(logging.DEBUG)

        # logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')
        # create logger
        # __name__ is the module’s name in the Python package namespace.
        self.logger = logging.getLogger(self.queue_name)

        # create logging api handler
        self.log_handler = LogHandler(
            self.tenant_id,
            self.service_name,
            self.url, self.default_logger)

        # create logging formatter
        self.formatter = logging.Formatter('%(asctime)s -'
                                           ' %(levelname)s - %(message)s')

        # add formatter to logging api
        self.log_handler.setFormatter(self.formatter)

        # set level to DEBUG
        self.logger.setLevel(logging.DEBUG)

        # Add handler to logger
        self.logger.addHandler(self.log_handler)

    def set_tenant(self, tenant):
        self.queue_name = self.service_name + tenant
        self.tenant_id = tenant
        self.log_handler.set_tenant(tenant)

    def info(self, message, send_mail=False):
        """Get INFO level message"""
        message = self.validate_message(message)
        if message is not None and message:
            msg = {"message": message, "send_mail": send_mail}
            self.logger.info(json.dumps(msg))

    def error(self, message, send_mail=False):
        """Get ERROR level message"""
        message = self.validate_message(message)
        if message is not None and message:
            msg = {"message": message, "send_mail": send_mail}
            self.logger.exception(json.dumps(msg))

    def warning(self, message, send_mail=False):
        """Get WARNING level message"""
        message = self.validate_message(message)
        if message is not None and message:
            msg = {"message": message, "send_mail": send_mail}
            self.logger.warning(json.dumps(msg))

    def debug(self, message, send_mail=False):
        """Get DEBUG level message"""
        message = self.validate_message(message)
        if message is not None and message:
            msg = {"message": message, "send_mail": send_mail}
            self.logger.debug(json.dumps(msg))

    @staticmethod
    def validate_message(message):
        """Validate message parameter"""
        message = str(message).strip()
        return message

    # Save the log message in the Cloudwatch log and save the value in the metric
    def log_metric(self, metric_name, value, message, send_mail=False):
        message = metric_name + " - " + message  # Concatenate metric name with msg

        self.metric_name = metric_name
        self.value = value
        self.info(message, send_mail)  # Get the Level of the message as INFO
        self.send_metric(self.value, self.metric_name)

    #  Create the metric and Save the provided value in the metric
    def send_metric(self, value, metric_name):
        data = {
            "metric_name": metric_name,
            "namespace": self.service_name,
            "value": value
        }
        metric_data = json.dumps(data)

        """Send the value to API"""

        try:
            response = requests.post(self.metric_url, data=metric_data)
            response.raise_for_status()
        except requests.exceptions.ConnectionError:
            self.default_logger.error("API Connection error occurred.")
        except requests.exceptions.Timeout:
            self.default_logger.error("Request timed out.")
        except requests.exceptions.HTTPError:
            self.default_logger.error("Request HTTP error")
        return
