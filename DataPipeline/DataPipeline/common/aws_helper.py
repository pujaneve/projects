#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Helper functions for AWS Services
"""
import json


__version__ = '0.1'

import boto3
import botocore
import botocore.exceptions
from DataPipeline.conf import config
import time


def get_s3_resource():
    """
    Get S3 client.
    :return: Boto s3 client Object
    """
    try:
        s3 = boto3.resource('s3')

        return s3
    except Exception:
        raise Exception("Invalid AWS credentials")


def get_s3_client():
    """
    Get S3 client.
    :return: Boto s3 client Object
    """
    try:
        s3 = boto3.client('s3')

        return s3
    except Exception:
        raise Exception("Invalid AWS credentials")


def get_lambda_resource():
    """
    Get Aws Lambda client.
    :return: Boto Lambda client Object
    """
    try:
        return boto3.client('lambda')
    except Exception:
        raise Exception("Invalid AWS credentials")


def get_s3_bucket(bucket_name, s3_resource):
    """
    Get S3 bucket Object.
    :param bucket_name: name of the bucket.
    :param s3_resource: s3 client
    :return: Boto s3 client Object
    """
    s3 = s3_resource
    if s3 is None:
        return
    try:
        bucket = s3.Bucket(bucket_name)
    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the bucket does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 404:
            raise Exception('S3 bucket {} does not exist'.format(bucket_name))
        raise e

    return bucket


def get_s3_object(bucket_name, key, s3_resource):
    """
    Get S3 Object.
    :param bucket_name: name of the bucket.
    :param key: name of file tn above bucket.
    :param s3_resource: Boto s3 client Object
    :return: S3 Object
    """
    s3 = s3_resource
    if s3 is None:
        raise Exception('Invalid Credentials')
    try:
        s3_object = s3.Object(bucket_name, key)
        return s3_object
    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the file does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 404:
            raise Exception('Specific key {} does not exist'.format(key))
        raise e


def create_s3_folder(bucket_name, folder_name, s3_resource):
    """
    Put Object into S3 bucket.
    :param bucket_name: name of the bucket.
    :param folder_name: name of file tn above bucket.
    :param s3_resource: Boto s3 client Object.
    :return: True
    """
    s3 = s3_resource
    if s3 is None:
        return
    try:
        s3_object = s3.Object(bucket_name, folder_name + "/")
        s3_object.put()

    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the file does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 404:
            raise Exception('Specific key {} does not exist'.format(folder_name))
        raise e

    return True


def put_s3_object(bucket_name, key, s3_resource, data):
    """
    Put Object into S3 bucket.
    :param bucket_name: name of the bucket.
    :param key: name of file tn above bucket.
    :param s3_resource: Boto s3 client Object.
    :param data :Binary Data
    :return: S3 Object
    """
    s3 = s3_resource
    if s3 is None:
        return
    try:

        s3_object = s3.Object(bucket_name, key)
        s3_object.put(Body=data)

    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the file does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 404:
            raise Exception('Specific key {} does not exist'.format(key))
        raise e

    return s3_object


def get_kinesis_client():
    """
    Get Aws kinesis client.
    :return: Boto kinesis client Object
    """
    try:
        return boto3.client('kinesis')
    except Exception as e:
        raise Exception(e)


def put_records_to_stream(stream_name, records, kinesis_client):
    """
    Publish Multiple records in to kinesis stream.
    :param stream_name: Name of the Kinesis Stream.
    :param records: List of records to be put onto Kinesis Stream.
    :param kinesis_client: Boto3 Kinesis Client Object.
    :return: Flag shows whether records successfully published or not.
    """
    is_published = False
    if kinesis_client is not None:
        try:
            kinesis_client.put_records(
                Records=records,
                StreamName=stream_name)
            is_published = True
        except Exception as e:
            # If a client error is thrown, then check that it was a 404 error.
            # If it was a 404 error, then the bucket does not exist.
            raise Exception(str(e))

    return is_published


def put_record_to_stream(stream_name, record, kinesis_client):
    """
    Publish Single record in to kinesis stream.
    :param stream_name: Name of the Kinesis Stream.
    :param record: Record to be put onto Kinesis Stream.
    :param kinesis_client: Boto3 Kinesis Client Object.
    :return: Flag shows whether records successfully published or not.
    """
    if kinesis_client is None:
        return
    try:
        kinesis_client.put_record(
            Data=record.encode(),
            PartitionKey='string',
            StreamName=stream_name)
        return True
    except Exception as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the bucket does not exist.
        raise Exception(str(e))


def invoke_lambda_async(lambda_client, function_name, request_data):
    """
    :param lambda_client: Boto3 lambda client object.
    :param function_name: lambda function name to be invoked.
    :param request_data: Json object to be send as a Input for lambda.
    :return: TRUE/FALSE
    """
    try:
        lambda_client.invoke(
            FunctionName=function_name,
            InvocationType='Event',
            Payload=json.dumps(request_data)
        )
        return True
    except Exception as e:
        raise e


def submit_spark_job(step=None, tenant_id=None):
    try:
        conn = boto3.client("emr", region_name=config.get_value("global.emr_region"))
        # chooses the first cluster which is Running or Waiting
        # possibly can also choose by name or already have the cluster id
        if tenant_id is None:
            raise Exception("Tenant should not be None")
        cluster_id = get_emr_cluster_for_tenant(tenant_id)
        step['Name'] = tenant_id + " " + step['Name'] + " " + time.strftime("%Y-%m-%d:%H:%M")
        conn.add_job_flow_steps(JobFlowId=cluster_id, Steps=[step])
        return "Job Submitted Successfully"
    except Exception as e:
        raise e


def get_emr_cluster_for_tenant(tenant_id):
    try:
        cluster_id = None
        conn = boto3.client("emr", region_name=config.get_value("global.emr_region"))
        # chooses the first cluster which is Running
        # possibly can also choose by name or already have the cluster id
        all_clusters = conn.list_clusters(ClusterStates=["RUNNING", "WAITING"])
        clusters = []
        # choose the correct cluster
        for c in all_clusters["Clusters"]:
            tags = conn.describe_cluster(ClusterId=c["Id"])['Cluster']['Tags']
            if len(tags) == 0:
                raise Exception("No valid EMR clusters available.")
            for tag in tags:
                if str(tag['Key']).lower() == str(config.get_value("global.emr_tag_key")).lower() \
                        and str(tag['Value']).lower() == str(config.get_value("global.emr_tag_value")).lower():
                    clusters.append(c)

        if len(clusters) == 0:
            raise Exception("No valid EMR clusters available.")

        running_clusters = [c["Id"] for c in clusters
                            if c["Status"]["State"] in ["RUNNING"]]

        # # take the first relevant cluster
        if running_clusters:
            for i in running_clusters:
                # list the steps by cluster_id
                steps = conn.list_steps(ClusterId=i)

                # list the running steps

                for s in steps["Steps"]:
                    if s["Status"]["State"] in ["RUNNING"]:
                        try:
                            tenant = str(s["Name"])[:len(tenant_id)]
                            if tenant == tenant_id:
                                return i
                        except Exception:
                            pass

                for s in steps["Steps"]:
                    if s["Status"]["State"] in ["PENDING"]:
                        try:
                            tenant = str(s["Name"])[:len(tenant_id)]
                            if tenant == tenant_id:
                                return i
                        except Exception:
                            pass

        if cluster_id is None:
            clusters = [c["Id"] for c in clusters
                        if c["Status"]["State"] in ["WAITING"]]
            if not clusters:
                raise Exception("No valid EMR clusters available.")
            else:
                return clusters[0]
    except Exception:
        raise Exception("No valid EMR clusters available.")


def remove_s3_object(bucket_name, key, s3_resource):
    """
    Put Object into S3 bucket.
    :param bucket_name: name of the bucket.
    :param key: name of file in above bucket.
    :param s3_resource: Boto s3 client Object.
    :return: S3 Object
    """
    s3 = s3_resource
    if s3 is None:
        return
    try:
        s3.Object(bucket_name, key).delete()

    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the file does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 404:
            raise Exception('Specific key {} does not exist'.format(key))
        raise e

    return True


def get_s3_objects_of_folder(bucket_name, folder_name, s3_resource):
    """
    Get S3 Object.
    :param bucket_name: name of the bucket.
    :param folder_name: name of folder tn above bucket.
    :param s3_resource: Boto s3 client Object
    :return: S3 Object
    """
    s3 = s3_resource
    all_object = None
    if s3 is None:
        return all_object
    try:
        bucket = s3.Bucket(bucket_name)
        all_object = bucket.objects.filter(Prefix=folder_name)
    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the file does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 404:
            raise Exception('Specific key {} does not exist'.format(folder_name))
        raise e
    return all_object


def send_email(sender=None, recipients=None, cc=None, subject=None, body=None):
    # The character encoding for the email.
    if recipients is None:
        recipients = []
    if cc is None:
        cc = []
    charset = "UTF-8"

    # Create a new SES resource and specify a region.
    client = boto3.client('ses')

    # Try to send the email.
    try:
        # Provide the contents of the email.
        client.send_email(
            Destination={
                'ToAddresses': recipients,
                'CcAddresses': cc,
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': charset,
                        'Data': body,
                    },
                },
                'Subject': {
                    'Charset': charset,
                    'Data': subject,
                },
            },
            Source=sender,
        )
    # Display an error if something goes wrong.
    except Exception as e:
        raise e


def key_existing_s3(s3_resource, bucket, key):
    """

    :param s3_resource: S3 object.
    :param bucket: bucket name.
    :param key: file name.
    :return: True/False
    """
    try:
        s3_resource.meta.client.head_object(Bucket=bucket, Key=key)
        return True
    except botocore.exceptions.ClientError:
        return False
    except Exception:
        return False


def get_cw_metric_resource():
    """
    Get Cloudwatch metric client
    :return: Boto3 Cloudwatch metric client Object
    """
    try:
        cloudwatch_metric = boto3.client('cloudwatch')
        return cloudwatch_metric
    except Exception as e:
        raise Exception(e)


def send_to_metric(metric_name, value, namespace):
    """

    :param metric_name: Name of the metric
    :param value: The value which needs to be put in the metric
    :param namespace: The metric will be saved under the specified namespace
    :return: Boto3 Cloudwatch metric client Object
    """
    try:
        cloudwatch = get_cw_metric_resource()
        response = cloudwatch.put_metric_data(MetricData=[
            {
                'MetricName': metric_name,
                'Unit': 'None',
                'Value': value
            },
        ],
            Namespace=namespace)
        return response

    except Exception as e:
        raise Exception(e)


def get_cw_log_resource():
    """
    Get Cloudwatch log client
    :return: Boto Cloudwatch log client Object
    """
    try:
        cloudwatch = boto3.client('logs')
        return cloudwatch
    except Exception as e:
        raise Exception(e)


def send_log(cloudwatch_client=None, **args):
    """
    #Send logs to cloudwatch
    :param cloudwatch_client Boto3 cloudwatch client
    :param args: dict of parameter
    :return: response
    """
    if cloudwatch_client is None:
        raise Exception("Cloudwatch client should not be none")
    response = cloudwatch_client.put_log_events(**args)
    return response


def create_log_group(log_group_name, cloudwatch_client=None):
    """
    create log group in cloudwatch.
    :param cloudwatch_client Boto3 cloudwatch client.
    :param log_group_name: name of log group which needs to be created.
    :return: response
    """
    if cloudwatch_client is None:
        raise Exception("Cloudwatch client should not be none")
    try:
        response = cloudwatch_client.create_log_group(logGroupName=log_group_name)
        return response
    except Exception as e:
        raise Exception(e)


def create_log_stream(log_group_name, log_stream_name, cloudwatch_client=None):
    """
    create log stream in cloudwatch.
    :param cloudwatch_client Boto3 cloudwatch client.
    :param log_group_name: name of log group.
    :param log_stream_name: name of log stream which needs to be created.
    :return: response.
    """
    if cloudwatch_client is None:
        raise Exception("Cloudwatch client should not be none")
    try:
        response = cloudwatch_client.create_log_stream(logGroupName=log_group_name,
                                                       logStreamName=log_stream_name)
        return response
    except Exception as e:
        raise e


def retention_policy(log_group_name, retention_in_days, cloudwatch_client=None):
    """
    Add retention policy to cloudwatch group.
    :param cloudwatch_client Boto3 cloudwatch client.
    :param log_group_name: Name of cloudwatch group.
    :param retention_in_days: Name of cloudwatch stream.
    :return: response

    """
    if cloudwatch_client is None:
        raise Exception("Cloudwatch client should not be none")
    try:
        response = cloudwatch_client.put_retention_policy(logGroupName=log_group_name,
                                                          retentionInDays=retention_in_days)
        return response
    except Exception as e:
        raise e


def create_master_key(kms_client, key_alias):
    """
    Create Customer Master Key (CMK) for KMS

    :return master keyId(ARN)
    """
    try:
        cmk_key_resp = kms_client.create_key()
        key_id = cmk_key_resp['KeyMetadata']['KeyId']
        kms_client.create_alias(
            AliasName='alias/' + key_alias,
            TargetKeyId=key_id
        )
        return key_id
    except botocore.exceptions.ClientError as e:
        raise e


def get_kms_key(kms_client, key_alias):
    key_id = None
    try:
        response = kms_client.list_aliases(
        )
        for rsp in response['Aliases']:
            if rsp['AliasName'] == 'alias/' + key_alias:
                key_id = rsp['TargetKeyId']
                return key_id
        return key_id
    except Exception as e:
        raise e


def create_and_store_data_key(kms_client, cmk_key_id, key_name):
    try:
        secret_client = get_secret_client()
        data_key_resp = kms_client.generate_data_key(
            KeyId=cmk_key_id, KeySpec='AES_256')

        try:
            response = secret_client.create_secret(
                Name=key_name,
                Description='Data security',
                KmsKeyId=cmk_key_id,
                SecretBinary=data_key_resp['CiphertextBlob']
            )
            return response
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == 'ResourceExistsException':
                response = secret_client.put_secret_value(
                    SecretId=key_name,
                    SecretBinary=data_key_resp['CiphertextBlob']
                )
                return response
            else:
                raise e
    except Exception as e:
        raise e


def get_kms_client():
    """
    Get S3 client.
    :return: Boto s3 client Object
    """
    try:
        kms = boto3.client('kms')

        return kms
    except Exception as e:
        raise e


def get_secret_client():
    """
    Get S3 client.
    :return: Boto s3 client Object
    """
    try:
        client = boto3.client('secretsmanager')
        return client
    except Exception as e:
        raise e


def get_data_key(key_name):
    secret_client = get_secret_client()
    kms_client = get_kms_client()
    try:
        secret_value = secret_client.get_secret_value(
            SecretId=key_name
        )
        return kms_client.decrypt(CiphertextBlob=secret_value['SecretBinary'])['Plaintext']
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':

            cmk_key = get_kms_key(kms_client, key_name)

            if cmk_key is None:
                cmk_key = create_master_key(kms_client, key_name)

            create_and_store_data_key(kms_client, cmk_key, key_name)
            secret_value = secret_client.get_secret_value(
                SecretId=key_name
            )
            return kms_client.decrypt(CiphertextBlob=secret_value['SecretBinary'])['Plaintext']
        else:
            raise e
    except Exception as e:
        raise e
