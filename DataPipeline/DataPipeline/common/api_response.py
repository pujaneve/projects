
# -*- coding: utf-8 -*-

"""
Class will construct and return API response in a consistent manner for all
microservices.
"""

__version__ = '0.1'

import uuid

from flask import jsonify


class ApiResponse(object):
    """
    Class for creating and sending API responses for Flask APIs
    """

    @staticmethod
    def _validate_params(request_id, status_code, msg, payload):
        """
        Basic validation of parameters (type checking)

        :param request_id: UUID of the request
        :param status_code: HTTP status code
        :param msg: Message that conveys the result of the API
        :param payload: Data that API sends back to the client in response
        :return:
        """
        assert isinstance(request_id, str)
        assert isinstance(status_code, int)
        assert isinstance(msg, str)
        if payload is not None:
            assert isinstance(payload, dict)

    @staticmethod
    def _send(request_id, status_code, msg, payload):
        """
        Create the json response for the request

        :param request_id: UUID of the request
        :param status_code: HTTP status code
        :param msg: Message that conveys the result of the API
        :param payload: Data that API sends back to the client in response
        :return: Flask response
        """
        default_resp = {
            'RequestId': request_id,
            'Message': msg,
            'StatusCode': status_code,
            'Data': None
        }
        if payload is not None:
            default_resp['Data'] = payload

        return jsonify(default_resp), status_code

    @classmethod
    def success(cls, request_id=str(uuid.uuid4()), status_code=200, msg="Success", payload=None):
        """
        This method should be used to send the response of successful operations.

        :param request_id: UUID of the request
        :param status_code: HTTP status code
        :param msg: Message that conveys the result of the API
        :param payload: Data that API sends back to the client in response
        :return: Flask API response
        """
        try:
            msg = str(msg)
            cls._validate_params(request_id, status_code, msg, payload)
        except AssertionError:
            status_code = 500
            msg = 'Internal Error Occurred'
            payload = None
        return cls._send(request_id, status_code, msg, payload)

    @classmethod
    def failure(cls, request_id=str(uuid.uuid4()), status_code=500, msg="Error", payload=None):
        """
        This method should be used to send responses when an operation fails

        :param request_id: UUID of the request
        :param status_code: HTTP status code
        :param msg: Message that conveys the result of the API
        :param payload: Data that API sends back to the client in response
        :return: Flask API response
        """

        try:
            msg = str(msg)
            cls._validate_params(request_id, status_code, msg, payload)
        except AssertionError:
            status_code = status_code
            msg = 'Internal Error Occurred'
            payload = None
        return cls._send(request_id, status_code, msg, payload)
