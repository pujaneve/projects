#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This module contains common validation methods required for validate input parameters
for REST APIs
"""

__version__ = '0.1'


def validate_schema(input_schema):
    """
    This method validate input_schema.
    :param input_schema: input schema to be validate.
    :return: True/False
    """
    is_valid = False
    if isinstance(input_schema, dict):
        if "type" not in input_schema or input_schema['type'] != 'struct':
            raise Exception("Invalid schema type.")
        if "fields" in input_schema \
                and isinstance(input_schema["fields"], list) \
                and len(input_schema["fields"]) > 0:
            return validate_fields_of_schema(input_schema["fields"])
    return is_valid


def validate_schema_metadata(schema_metadata):
    """
    This method validate input metadata to be update.
    :param schema_metadata: input metadata to be validate.
    :return: True/False
    """
    if isinstance(schema_metadata, dict):
        if "base_data_path" not in schema_metadata \
                or "compatible_schema_versions" not in schema_metadata \
                or "current_schema_version" not in schema_metadata \
                or "incompatible_schema_versions" not in schema_metadata \
                or "latest_version_no" not in schema_metadata \
                or "mandatory_fields" not in schema_metadata:
            return False
        if not isinstance(schema_metadata["base_data_path"], str) or not validate(schema_metadata["base_data_path"]) \
                or (schema_metadata["current_schema_version"] is not None and (
                not isinstance(schema_metadata["current_schema_version"], str)
                or not validate(schema_metadata["current_schema_version"]))) \
                or (not isinstance(schema_metadata["compatible_schema_versions"], list)) \
                or not isinstance(schema_metadata["incompatible_schema_versions"], list) \
                or not isinstance(schema_metadata["latest_version_no"], int) \
                or not isinstance(schema_metadata["mandatory_fields"], list):
            return False
        return validate_fields_of_schema(schema_metadata["mandatory_fields"])
    else:
        raise Exception("Invalid Input Json")


def validate_fields_of_schema(fields):
    """
    This method validate list of fields for an schema.
    :param fields: List of fields to be validate.
    :return: True / False.
    """
    for field in fields:
        if "name" not in field \
                or "type" not in field \
                or "nullable" not in field \
                or "metadata" not in field:
            raise Exception("Invalid schema fields.")
        if not isinstance(field["name"], str) \
                or not isinstance(field["type"], str) \
                or not isinstance(field["nullable"], bool) \
                or not isinstance(field["metadata"], dict):
            raise Exception("Invalid schema fields.")
    return True


def validate_de_dup_metadata(de_dup_metadata):
    """
    This method validate input metadata to be update.
    :param de_dup_metadata: input de_dup_metadata to be validate.
    :return: True/False
    """
    if isinstance(de_dup_metadata, dict):
        if "dup_idfy_cols" not in de_dup_metadata \
                or "sort_by_cols" not in de_dup_metadata:
            return False
        if not isinstance(de_dup_metadata["dup_idfy_cols"], list) \
                or not isinstance(de_dup_metadata["sort_by_cols"], list):
            return False
        return validate_fields_of_de_dup_metadata(de_dup_metadata["sort_by_cols"])
    else:
        raise Exception("Invalid Input Json")


def validate_fields_of_de_dup_metadata(fields):
    """
    This method validate list of fields for an schema.
    :param fields: List of fields to be validate.
    :return: True / False.
    """
    for field in fields:
        if "col_name" not in field \
                or "type" not in field:
            raise Exception("Invalid Input Json.")
        if not isinstance(field["col_name"], str) \
                or not isinstance(field["type"], str):
            raise Exception("Invalid Input Json")
    return True


def validate(params):
    if params is not None and params and not str(params).isspace():
        return True
    return False
