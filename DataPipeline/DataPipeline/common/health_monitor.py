import psutil
from DataPipeline.common import logging_agent
from DataPipeline.conf import config
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
import atexit


class HealthMonitor(object):

    def __init__(self, group_name, service_name):
        self.group_name = group_name
        self.service_name = service_name

    def start_monitoring(self):
        scheduler = BackgroundScheduler()
        scheduler.start()

        scheduler.add_job(
            func=self.collect_metric,
            args=[self.group_name, self.service_name],
            trigger=IntervalTrigger(minutes=config.get_value("monitoring.Monitoring_time_interval_in_min")),
            id='Monitoring' + self.group_name + self.service_name,
            name='Monitoring' + self.group_name + self.service_name,
            replace_existing=True)
        atexit.register(lambda: scheduler.shutdown())

    @staticmethod
    def collect_metric(tenant, service):
        # CPU Utilization in percentage

        logger = logging_agent.LoggingHelper(service=service, tenant=tenant).get_logger()
        cpu_percent = psutil.cpu_percent(interval=0.1)
        logger.log_metric("CPUUtilization", cpu_percent,
                          "{} - CPU utilization - {}%".format(service, cpu_percent))

        # Physical memory available
        mem = psutil.virtual_memory()
        mem_avl = bytes_to_gb(mem.available)
        logger.log_metric("MemoryAvailable", mem_avl, "{} - Memory Available - {}GB".format(service, mem_avl))

        # Physical memory used
        mem_used = bytes_to_gb(mem.used)
        logger.log_metric("MemoryUsed", mem_used, "{} - Memory used - {}GB".format(service, mem_used))

        # Disk Usage
        disk_usage = psutil.disk_usage('/')
        disk_used = bytes_to_gb(disk_usage.used)
        logger.log_metric("DiskSpaceUsed", disk_used,
                          "{} - Disk space used - {}GB".format(service, disk_used))
        disk_used_pert = disk_usage.percent
        logger.log_metric("DiskSpaceUsedPercent", disk_used_pert,
                          "{} - Disk space used - {}%".format(service, disk_used_pert))

        # Disk I/O counters
        net_io = psutil.net_io_counters()
        bytes_sent = bytes_to_gb(net_io.bytes_sent)
        logger.log_metric("BytesSent", bytes_sent,
                          "{} - Number of bytes sent over network - {}GB".format(service, bytes_sent))
        bytes_recv = bytes_to_gb(net_io.bytes_recv)
        logger.log_metric("BytesReceived", bytes_recv,
                          "{} - Number of bytes received over network - {}GB".format(service, bytes_recv))
        packets_sent = net_io.packets_sent
        logger.log_metric("PacketsSent", packets_sent,
                          "{} - Number of packets sent over network - {}".format(service, packets_sent))
        packets_recv = net_io.packets_recv
        logger.log_metric("PacketsReceived", packets_recv,
                          "{} - Number of packets received over network - {}".format(service, packets_recv))
        errin = net_io.errin
        logger.log_metric("NumberOfErrorsWhileReceiving", errin,
                          "{} - Total number of errors while receiving - {}".format(service, errin))
        errout = net_io.errout
        logger.log_metric("NumberOfErrorsWhileSending", errout,
                          "{} - Total number of errors while sending - {}".format(service, errout))
        dropin = net_io.dropin
        logger.log_metric("IncomingPacketsWhichDropped", dropin,
                          "{} - Total number of incoming packets which were dropped - {}".format(service,
                                                                                                 dropin))
        dropout = net_io.dropout
        logger.log_metric("OutgoingPacketsWhichDropped", dropout,
                          "{} - Total number of outgoing packets which were dropped - {}".format(service,
                                                                                                 dropout))


def bytes_to_gb(value, precision=4):
    if value > 0:
        value = value / (1024 * 1024 * 1024)  # apply the division
    return float("%.*f" % (precision, value))
