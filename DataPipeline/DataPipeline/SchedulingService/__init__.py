# Copyright © 2018, Proxima Analytics, LLC, All rights reserved.
# THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF PROXIMA ANALYTICS,
# ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER IS PROHIBITED.

"""
Initialize Scheduling service
"""
__author__ = 'Vigoursoft Global Solutions'
__copyright__ = 'Copyright © 2018, Proxima Analytics, LLC'
__license__ = 'Proprietary. See LICENSE file'
__version__ = '0.1'

