from DataPipeline.SchedulingService.generic_scheduler_service import app
from DataPipeline.SchedulingService.generic_scheduler_utils import GenericSchedulingService
from DataPipeline.conf import config

if __name__ == '__main__':
    GenericSchedulingService(service=config.get_value("generic_scheduling.log_stream")) \
        .start_scheduler()

    app.run(host=config.get_value("services_host.generic_scheduling"),
            port=config.get_value("services_port.generic_scheduling"))
