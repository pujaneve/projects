

"""
Scheduling service
"""

__version__ = '0.1'

import atexit
import json
import os
import uuid
from datetime import datetime

import requests
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from DataPipeline.common import aws_helper
from DataPipeline.common.logging_agent import LoggingHelper
from DataPipeline.conf import config

CUSTOMER_DATAMART = "Customer-Datamart"
TENANT_ID = "tenant"
REQUEST_ID = "DatamartRequestID"
START_DATE = "start_date"
END_DATE = "end_date"
FREQUENCY = "frequency"
EXECUTION_TIME = "execution_time"
SCHEDULING_INFO = "scheduling_info"
WEEKLY = "weekly"
DAILY = "daily"
MONTHLY = "monthly"
JOBID = "JobID"
API_INFO = "api_info"
API_URL = "api_url"
API_ARGUMENTS = "api_arguments"
METHOD_TYPE = "method_type"
API_DATA = "api_data"

day_dict = {"monday": 0,
            "tuesday": 1,
            "wednesday": 2,
            "thursday": 3,
            "friday": 4,
            "saturday": 5,
            "sunday": 6}

scheduler = BackgroundScheduler()


class GenericSchedulingService(object):
    """
    Generic scheduling service class
    """

    def __init__(self, service="Generic Scheduling", tenant="Other"):
        """
        Instantiate scheduling class
        """
        self.S3Bucket = config.get_value("generic_scheduling.s3_bucket")
        self.S3Prefix = None
        self.logger = LoggingHelper(service=service, tenant=tenant).get_logger()
        self.logger.info("Starting Scheduling service")
        self.logger.info("Scheduling service successfully started.")

    def start_scheduler(self):
        """
        Start APScheduler in background
        """
        try:
            scheduler.start()
            resp = self.read_s3_files()
            if resp:
                return resp
        except Exception as e:
            self.logger.error("Exception [%s] while starting scheduler in scheduling service." % e)
            return e
        return resp

    def read_s3_files(self):
        """
        Read all jobs earlier stored on S3 when scheduled.
        On restart of scheduling service, the earlier scheduled jobs will be lost and thus new jobstore for the same
        scheduled jobs will be created by reading S3 job files.
        :return:If successful return None, otherwise return ERROR message
        """
        try:
            s3_client = aws_helper.get_s3_client()
            list_obj_resp = s3_client.list_objects(Bucket=self.S3Bucket)

            key_list = list_obj_resp.get('Contents')
            if key_list is None:
                self.logger.info('No config files found')
                return

            for obj_info in list_obj_resp['Contents']:
                key = obj_info['Key']
                temp = key.split('/')[1]
                if temp == "":
                    continue
                self.S3Prefix = key.split('/')[0]
                obj_resp = s3_client.get_object(Bucket=self.S3Bucket, Key=key)
                content = obj_resp['Body'].read()
                job_info = json.loads(content)
                job_id = job_info[JOBID]
                resp = self.schedule_job(None, self.S3Prefix, job_info, job_id)
                if resp:
                    return resp
        except Exception as e:
            self.logger.error("Exception [%s] while reading jobs from S3 in scheduling service." % e)
            return e
        return None

    def schedule_job(self, api_request_id, s3_prefix, job_info, job_id):
        """
        Schedule job and add it to S3 with specified service prefix for given tenant
        :param api_request_id: REquestId of an schedule_job API
        :param s3_prefix: Service prefix to store job on S3
        :param job_info: Job information containing scheduling info
        :param job_id: Associated jobid with already scheduled job
        :return: If successful return None, otherwise return ERROR message
        """
        try:
            msg = None
            self.S3Prefix = s3_prefix
            tenant_id = job_info[TENANT_ID]

            scheduling_info = job_info[SCHEDULING_INFO]

            for key, value in scheduling_info.items():
                if key == START_DATE or key == END_DATE:
                    msg = self._validate_date(api_request_id, value)
                    if msg:
                        return msg

            scheduler_weekday = None
            scheduler_month_date = None
            random = 0

            day_list = self._get_day_list()

            if scheduling_info[START_DATE] == scheduling_info[END_DATE] or \
                            scheduling_info[FREQUENCY].lower() == DAILY:
                scheduler_month_date = '1-31'
                exec_time = scheduling_info[DAILY]
                msg, random_value = self._schedule_job_for_exec_time(api_request_id, tenant_id, job_info,
                                                                     job_id,
                                                                     exec_time, scheduler_weekday, scheduler_month_date,
                                                                     random)
            elif scheduling_info[FREQUENCY].lower() == WEEKLY:
                for scheduler_weekday, exec_time in scheduling_info[WEEKLY].items():
                    if scheduler_weekday.lower() not in day_list:
                        msg = "Invalid week name %s" % scheduler_weekday
                        self.logger.error("APIRequestId : %s, API: Schedule Job, Message:  %s!" % (api_request_id, msg))
                        return msg
                    else:
                        msg, random_value = self._schedule_job_for_exec_time(api_request_id, tenant_id,
                                                                             job_info,
                                                                             job_id, exec_time, scheduler_weekday,
                                                                             scheduler_month_date, random)
                        if msg:
                            return msg
                        random = random_value

            # if scheduling_info[FREQUENCY].lower() == MONTHLY:
            else:
                for scheduler_month_date, exec_time in scheduling_info[MONTHLY].items():
                    if not 1 <= int(scheduler_month_date) <= 31:
                        msg = "Invalid date of month %s" % scheduler_month_date
                        self.logger.error("APIRequestId : %s, API: Schedule Job, Message: %s!" % (api_request_id, msg))
                        return msg
                    else:
                        msg, random_value = self._schedule_job_for_exec_time(api_request_id, tenant_id,
                                                                             job_info,
                                                                             job_id, exec_time, scheduler_weekday,
                                                                             scheduler_month_date, random)
                        if msg:
                            return msg
                        random = random_value

            return msg
        except Exception as e:
            self.logger.error("Exception [%s] to schedule job in scheduler" % e)
            return e

    def _schedule_job_for_exec_time(self, api_request_id, tenant_id, job_info, job_id, exec_time,
                                    scheduler_weekday, scheduler_month_date, random):
        """
        Schedules separate job for multiple execution times and days or dates.
        :param api_request_id: Request id of an API
        :param tenant_id: tenant
        :param job_info: job info
        :param job_id: jobid associated with already scheduled job, otherwise None if not already shceduled
        :param exec_time: tuple of execution time for specified day of week or date of month
        :param scheduler_weekday: week day if scheduler frequency is WEEKLY
        :param scheduler_month_date: date of month if scheduler frequemcy is MONTHLY
        :param random: an integer value for creating new version of same tenant for jobs in S3
        :return: error message on failure, otherwise None
        """
        time_tuple = ()
        scheduler_day = None

        try:
            if self.S3Prefix == CUSTOMER_DATAMART:
                request_id = job_info[REQUEST_ID]
            else:
                request_id = str(uuid.uuid4())

            if scheduler_weekday:
                scheduler_day = day_dict[scheduler_weekday.lower()]

            s_date = job_info[SCHEDULING_INFO][START_DATE]
            e_date = job_info[SCHEDULING_INFO][END_DATE]
            msg = self._compare_start_end_dates(api_request_id, s_date, e_date)
            if msg:
                return msg, None

            execute_api = job_info[API_INFO][API_URL]
            api_arguments = job_info[API_INFO][API_ARGUMENTS]
            method_type = job_info[API_INFO][METHOD_TYPE]
            api_data = json.loads(job_info[API_INFO][API_DATA])

            reason = self._validate_execution_time(api_request_id, exec_time)
            if reason:
                return reason, None

            for x in exec_time:
                tm = x.split(':')
                time_tuple = time_tuple + (tm,)

            for item in time_tuple:
                job_time = []
                exectime = item[0] + ":" + item[1]
                job_time.append(exectime)
                if not job_id:
                    random = random + 1
                    sc_jobid = tenant_id + "_" + request_id + "_" + str(random)
                    job_info[JOBID] = sc_jobid
                else:
                    sc_jobid = job_id

                if s_date == e_date:
                    year, month, day = (str(x) for x in s_date.split('-'))
                    scheduler.add_job(
                        func=self._call_api,
                        args=[execute_api, api_arguments, method_type, api_data],
                        trigger=CronTrigger(start_date=s_date, hour=item[0], minute=item[1],
                                            day=day, month=month, year=year),
                        name=self.S3Prefix,
                        id=sc_jobid,
                        replace_existing=True)
                    job_info[SCHEDULING_INFO][DAILY] = job_time
                else:
                    if job_info[SCHEDULING_INFO][FREQUENCY].lower() == DAILY:
                        scheduler_day = '0-6'

                    scheduler.add_job(
                        func=self._call_api,
                        args=[execute_api, api_arguments, method_type, api_data],
                        trigger=CronTrigger(start_date=s_date, end_date=e_date, hour=item[0], minute=item[1],
                                            day_of_week=scheduler_day, day=scheduler_month_date),
                        name=self.S3Prefix,
                        id=sc_jobid,
                        replace_existing=True)

                    if job_info[SCHEDULING_INFO][FREQUENCY] == WEEKLY:
                        job_info[SCHEDULING_INFO][WEEKLY] = {scheduler_weekday: job_time}
                    elif job_info[SCHEDULING_INFO][FREQUENCY] == MONTHLY:
                        job_info[SCHEDULING_INFO][MONTHLY] = {scheduler_month_date: job_time}
                    else:
                        job_info[SCHEDULING_INFO][DAILY] = job_time

                res = self.add_job_to_s3(api_request_id, self.S3Prefix, job_info)
                if res:
                    msg = "Failed to add job info to S3 for JobID = %s!" % sc_jobid
                    self.logger.info("api_request_id : %s, API: Schedule Job, Message: %s " % (api_request_id, msg))
                    return msg, None
                atexit.register(lambda: scheduler.shutdown())

            scheduler.print_jobs()
            return msg, random
        except Exception as e:
            self.logger.error("Exception [%s] in _schedule_job_for_exec_time() in scheduling service." % e)
            return e, None

    def _validate_execution_time(self, api_request_id, execution_time):
        """
        Validate execution time.
        :param api_request_id: API requestsId
        :param execution_time: execution time list
        :return: Error message on Failure otherwise None
        """
        reason = None
        try:
            for value in execution_time:
                try:
                    datetime.strptime(value, "%H:%M")
                except ValueError:
                    reason = "time data does not match 24 hours format H:M"
                    self.logger.info("api_request_id : %s, Message: %s" % (api_request_id, reason))
                    return reason
            return reason
        except Exception as e:
            self.logger.error("Exception [%s] in validating execution time in scheduling service." % e)
            return e

    def _validate_date(self, api_request_id, input_date):
        """
        Validate date.
        :param api_request_id:  API requestsId
        :param input_date: start date or end date
        :return: Error message on Failure otherwise None
        """
        try:
            datetime.strptime(input_date, '%Y-%m-%d')
        except ValueError:
            message = "Incorrect scheduling date or its format , should be YYYY-MM-DD"
            self.logger.error("api_request_id : %s, Message: %s " % (api_request_id, message))
            return message
        return None

    def _compare_start_end_dates(self, api_request_id, start_date, end_date):
        """
        Compare dates such that End date should always be greater than Start date
        :param api_request_id: API requestsId
        :param start_date: scheduling start date
        :param end_date: scheduling end date
        :return: Error message on Failure otherwise None
        """
        if end_date < start_date:
            message = "End date [%s] should be greater than start date [%s]!" % (end_date, start_date)
            self.logger.error("api_request_id : %s, Message: %s " % (api_request_id, message))
            return message
        return None

    def delete_scheduled_job(self, api_request_id, job_id, prefix):
        """
        Delete scheduled job from scheduler jobstore as well as from S3.
        :param api_request_id: API request id
        :param job_id: associated job id of job
        :param prefix: service prefix.
        :return: Error message on failure , otherwise None
        """
        reason = None
        try:
            scheduler.remove_job(job_id)
            s3_resource = aws_helper.get_s3_resource()
            request_path = os.path.join(prefix, job_id + '.json')
            aws_helper.remove_s3_object(self.S3Bucket, request_path, s3_resource)
            scheduler.print_jobs()
        except Exception as e:
            reason = "Exception [%s] while deleting scheduled job" % e
            self.logger.error("api_request_id : %s, Message: %s" % (api_request_id, reason))

        return reason

    @staticmethod
    def _get_day_list():
        """
        Get list of days
        :return: list of days
        """
        day_list = []
        for day, value in day_dict.items():
            day_list.append(day)
        return day_list

    def add_job_to_s3(self, api_request_id, service_prefix, job_info):
        """
        Add job to S3
        :param api_request_id: Request Id of API
        :param service_prefix: Service prefix
        :param job_info: job details
        :return: If successful, return None otherwise ERROR message
        """
        try:
            jobid = job_info[JOBID]
            s3_resource = aws_helper.get_s3_resource()
            request_path = os.path.join(service_prefix, jobid + '.json')

            data = json.dumps(job_info, indent=4, sort_keys=True, default=str)

            new_obj = aws_helper.put_s3_object(self.S3Bucket, request_path, s3_resource, data)
            if not new_obj:
                msg = "New S3 object creation failed in scheduler!"
                self.logger.error("api_request_id : %s, Message: %s!" % (api_request_id, msg))
                return msg
            return None
        except Exception as e:
            self.logger.error("Exception [%s] while addign job to S3 in scheduling service." % e)
            return e

    def _call_api(self, api_url, api_arguments, method_type, api_data):
        """
        Execute API on scheduled job trigger
        :param api_url: API URL for execution
        :param api_arguments: API Arguments
        :param method_type: type of method such as GET, POST,PUT,DELETE
        :param api_data: Json data as input
        """
        try:
            self.logger.info(
                "Job triggered at %s and executing API [%s]" % (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), api_url))
            s = requests.Session()
            req = requests.Request(method=method_type, url=api_url, params=api_arguments, json=api_data)
            prepared_request = s.prepare_request(req)
            resp = s.send(prepared_request)
            if not resp:
                self.logger.error("Failed the execution of Query Management service!")
                return
            self.logger.info("Successfully executed query management service, here is the result : %s " % resp)
        except Exception as e:
            self.logger.error("Exception [%s] while calling API [%s] on job trigger at %s in scheduling service" % (
                e, api_url, datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            raise e
