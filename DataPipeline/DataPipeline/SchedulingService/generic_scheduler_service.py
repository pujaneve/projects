import uuid

from flask import Flask, request

from DataPipeline.SchedulingService.generic_scheduler_utils import GenericSchedulingService
from DataPipeline.common.api_response import ApiResponse
from DataPipeline.common.logging_agent import LoggingHelper
from DataPipeline.conf import config

app = Flask(__name__)

EXCEPTION = "Exception"

scheduling_log_stream = config.get_value("generic_scheduling.log_stream")


@app.route('/scheduler/status', methods=['GET'])
def status():
    """
    GET method checking status of service.
    :return: Status Success/Error
    """
    try:
        return ApiResponse.success(payload=None)
    except Exception as e:
        return ApiResponse.failure(msg=e)


@app.route('/schedulejob', methods=['POST'])
def job_scheduler():
    """
    API to schedule job and add it on S3
    :return: Success or Failure
    """
    api_request_id = str(uuid.uuid4())
    prefix = request.args['service']
    job_info = request.json
    tenant = job_info['tenant']

    obj = GenericSchedulingService(tenant=tenant, service=scheduling_log_stream)

    try:
        resp = obj.schedule_job(api_request_id, prefix, job_info, None)
        if resp:
            obj.logger.error("api_request_id: %s, API:schedulejob, Failed to schedule job in scheduler!" % (
                api_request_id))
            return ApiResponse.failure(api_request_id, 404, 'Failure', {"Reason": resp})
        obj.logger.info("api_request_id: %s, API:schedulejob, Successfully scheduled the job for tenant (%s)!" % (
            api_request_id, tenant))
        return ApiResponse.success(api_request_id, 200, 'Success')
    except Exception as e:
        obj.logger.error("api_request_id: %s, API:schedulejob, Exception : %s " % (api_request_id, e))
        return ApiResponse.failure(api_request_id, 404, 'Failure', {EXCEPTION: e})


@app.route('/deletejob', methods=['DELETE'])
def delete_job():
    """
    API to delete scheduled job from job store and S3.
    :return: Success or Failure
    """
    api_request_id = str(uuid.uuid4())
    prefix = request.args['service']
    job_id = request.args['JobID']
    log_agent = LoggingHelper('delete_scheduled_job', config.get_value("generic_scheduling.log_stream"))
    logger = log_agent.get_logger()
    try:
        obj = GenericSchedulingService(service=scheduling_log_stream)
        resp = obj.delete_scheduled_job(api_request_id, job_id, prefix)
        if resp:
            logger.error("api_request_id: %s, API:deletejob, Failed to delete scheduled job from scheduler!" % (
                api_request_id))
            return ApiResponse.failure(api_request_id, 404, 'Failure', {"Reason": resp})

        logger.info("api_request_id: %s, API:deletejob, Successfully deleted scheduled job from scheduler" % (
            api_request_id))
        return ApiResponse.success(api_request_id, 200, 'Success')
    except Exception as e:
        logger.error("api_request_id: %s, API:deletejob, Exception : %s " % (api_request_id, e))
        return ApiResponse.failure(api_request_id, 404, 'Failure', {EXCEPTION: e})


@app.route('/addjob', methods=['POST'])
def add_job_to_s3():
    """
    API to add job on S3
    :return: Success or Failure
    """
    api_request_id = str(uuid.uuid4())
    prefix = request.args['service']
    job_info = request.json
    tenant = job_info['tenant']
    obj = GenericSchedulingService(tenant=tenant, service=scheduling_log_stream)

    try:
        resp = obj.add_job_to_s3(api_request_id, prefix, job_info)
        if resp:
            obj.logger.error("api_request_id: %s, API:addjob, Failed to add job in S3!" % (
                api_request_id))
            return ApiResponse.failure(api_request_id, 404, 'Failure', {"Reason": resp})

        obj.logger.info("api_request_id: %s, API:addjob, Successfully added job in S3!" % (
            api_request_id))
        return ApiResponse.success(api_request_id, 200, 'Success')
    except Exception as e:
        obj.logger.error("api_request_id: %s, API:addjob, Exception : %s " % (api_request_id, e))
        return ApiResponse.failure(api_request_id, 404, 'Failure', {EXCEPTION: e})
