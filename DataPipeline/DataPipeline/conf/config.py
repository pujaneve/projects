#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This script is used to load scheduling.json and will be helpful in development and deployment.
"""


__version__ = '0.1'

import json
import os

import requests

try:
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../conf.json')) as json_data_file:
        data = json.load(json_data_file)


    def get_conf_value(key):
        value = data
        for k in key.split("."):
            if k in value:
                value = value[k]
            else:
                value = None
                break
        return value
except Exception as e:
    raise Exception("Error in config file: Invalid JSON Object")


def get_value(key):
    try:
        url = get_conf_value("config_service_end_point") + "/config?key=" + key

        resp = requests.get(url=url)
        if resp.status_code == 200:
            return json.loads(resp.content.decode())["Data"]["value"]
        else:
            raise Exception(json.loads(resp.content.decode())["Message"])
    except Exception:
        raise Exception("No response found from configuration service for")
