"""
Tic Tac Toe game for 2 users
"""

from IPython.display import clear_output
import random


def display_board(board):
    clear_output()  # Clear the previous board
    print(board[7] + "|" + board[8] + "|" + board[9])
    print(board[4] + "|" + board[5] + "|" + board[6])
    print(board[1] + "|" + board[2] + "|" + board[3])


def player_input():
    marker = ''
    while marker != 'X' and marker != 'O':
        marker = input("Player 1: What do you want X or O?").upper()
    if marker == 'X':
        return 'X', 'O'
    else:
        return 'O', 'X'


def place_marker(board, marker, position):
    board[position] = marker


def win_check(board, marker):
    #  Check the win possibilities for all rows, all columns and two diagonals
    return (board[1] == marker and board[2] == marker and board[3] == marker) or (
        board[4] == marker and board[5] == marker and board[6] == marker) or (
               board[7] == marker and board[8] == marker and board[9] == marker) or (
               board[1] == marker and board[4] == marker and board[7] == marker) or (
               board[2] == marker and board[5] == marker and board[8] == marker) or (
               board[3] == marker and board[6] == marker and board[9] == marker) or (
               board[1] == marker and board[5] == marker and board[9] == marker) or (
               board[3] == marker and board[5] == marker and board[7] == marker)


def choose_first():
    flip = random.randint(0, 1)
    if flip == 0:
        return 'Player_1'
    else:
        return 'Player_2'


def space_check(board, position):
    return board[position] == ' '


def full_board_check(board):
    for i in range(1, 10):
        if space_check(board, i):
            return False
    # will return True when board is full
    return True


def player_choice(board):
    position = 0
    while position not in [1, 2, 3, 4, 5, 6, 7, 8, 9] or not space_check(board, position):
        position = int(input("Choose a position between 1-9"))
    return position


def replay():
    choice = input("Do you want to play again? Enter Yes or No")
    return choice == 'Yes'


# While loop to keep running the game
print("Welcome to the Game")
while True:
    the_board = [' '] * 10
    player1_marker, player2_marker = player_input()
    turn = choose_first()
    print("{} will play first".format(turn))

    play_game = input("Ready to play? y or n?")
    if play_game == 'y':
        game_on = True
    else:
        game_on = False

    while game_on:

        if turn == 'Player_1':
            # player 1 turn
            display_board(the_board)  # Display Board
            position = player_choice(the_board)
            place_marker(the_board, player1_marker, position)
            if win_check(the_board, player1_marker):
                display_board(the_board)
                print("Player 1 has won")
                game_on = False
            else:
                if full_board_check(the_board):
                    display_board(the_board)
                    print("Tie Game!")
                    game_on = False
                else:
                    turn = 'Player_2'

        else:
            # player 2 turn
            display_board(the_board)
            position = player_choice(the_board)
            place_marker(the_board, player2_marker, position)
            if win_check(the_board, player2_marker):
                display_board(the_board)
                print("Player 2 has won")
                game_on = False
            else:
                if full_board_check(the_board):
                    display_board(the_board)
                    print("Tie Game!")
                    game_on = False
                else:
                    turn = 'Player_1'
    # Ask the user to replay the game
    if not replay():
        break  # Break out of the while loop on replay()
